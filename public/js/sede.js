$(document).ready(function() {

    idSX = $("#sedeX_id").val();

    if (idSX) {
        $.get('/sedesAll/', function(data) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == idSX) {
                    $("#sede_id").append("<option value='" + data[i].id + "' selected>" + data[i].nomSede + "</option>");
                } else {
                    $("#sede_id").append("<option value='" + data[i].id + "'>" + data[i].nomSede + "</option>");
                }
            }
        });

    } else {
        $.get('/sedesAll/', function(data) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == $("#sede_id_sesion").val()) {
                    $("#sede_id").append("<option value='" + data[i].id + "' selected>" + data[i].nomSede + "</option>");
                }
            }
        });
    }
});