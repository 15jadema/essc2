$(document).ready(function() {
    id_per = $("#periodo_id_text").val();
    $.get('/periodoOnly/' + 1, function(data) {
        $("#periodo_id").empty();
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id_per) {
                $("#periodo_id ").append("<option value='" + data[i].id + "' selected>" + data[i].nomPeriodo + "</option>");
            } else {
                $("#periodo_id ").append("<option value='" + data[i].id + "'>" + data[i].nomPeriodo + "</option>");
            }
        }
    });
});