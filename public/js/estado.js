$(document).ready(function() {

    id_est = $("#estado_id_text").val();
    $.get('/estado/' + 1, function(data) {
        $("#status_id").empty();
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id_est) {
                $("#status_id ").append("<option value='" + data[i].id + "' selected>" + data[i].nomEstado + "</option>");
            } else {
                $("#status_id ").append("<option value='" + data[i].id + "'>" + data[i].nomEstado + "</option>");
            }
        }
    });

})