$(document).ready(function() {
    id_pro = $("#programa_id_text").val();
    $.get('/programaOnly/' + 1, function(data) {
        $("#programa_id").empty();
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id_pro) {
                $("#programa_id ").append("<option value='" + data[i].id + "' selected>" + data[i].nomPrograma + "</option>");
            } else {
                $("#programa_id ").append("<option value='" + data[i].id + "'>" + data[i].nomPrograma + "</option>");
            }
        }
    });
});