$(document).ready(function() {
    id_unidad = $("#unidadAprendizaje_id_text").val();
    $.get('/unidad/' + 1, function(data) {
        $("#unidadAprendizaje_id").empty();
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id_unidad) {
                $("#unidadAprendizaje_id ").append("<option value='" + data[i].id + "' selected>" + data[i].nomUnidadAprendizaje + "</option>");
            } else {
                $("#unidadAprendizaje_id ").append("<option value='" + data[i].id + "'>" + data[i].nomUnidadAprendizaje + "</option>");
            }
        }
    });
});