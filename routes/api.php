<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/notas/getUnits/{group}', 'notasController@getUnits');
Route::get('/notas/getGroups', 'notasController@getGroups');
Route::get('/notas/getAll/{group}', 'notasController@getAll');
Route::post('/notas/actualizar/{user}/{codg}', 'notasController@updateNote');
Route::post('/notas/crear', 'notasController@addNote');
Route::post('/notas/eliminar/{user}/{codg}', 'notasController@deletedNote');