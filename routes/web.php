<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('student','studentController');

Route::get('/documentos/{id}','studentController@mostrarDoc');

Route::get('/municipios/{id}','studentController@mostrar');

Route::get('/municipios','studentController@mostrar1');

Route::get('/programas/{id}','studentController@mostrarPro');

Route::get('/cursos/{id}','studentController@mostrarCurso');

Route::get('/estudiantesdelcurso/{id}','studentController@mostrarstudentdecurso');

//Route::post('/guardar','studentController@guardar');

Route::post('/guardar','registroController@guardar');
	
//Route::post('/usuarios/crear', 'UserController@store');

Route::get('/estudiantesnotas/{id}','studentController@mostrarnotascurso');

Route::get('/docenteCursoX/{id}','cursoController@createCurso');

//* Consulta docentes activos, por sede /
Route::get('/docenteCurso/{id}','docenteController@docenteCurso');

//Route::get('/notas/{id}','studentController@mostrarNota');

Route::get('/genero/{id}', 'studentController@mostrarGenero');

Route::get('/estado/{id}', 'studentController@mostrarEstado');

Route::get('/docente/{id}', 'docenteController@mostrarDocentes');

Route::get('/unidad/{id}', 'unidadAprendizaje@mostrarUnidades');

Route::get('/periodoOnly/{id}', 'cursoController@mostrarPeriodo');

Route::get('/programaOnly/{id}', 'programaController@mostrarProgramas');

Route::get('/jornada/{id}', 'jornadaController@mostrarJornadas');

Route::get('/municipiosAll/', 'studentController@mostrarMunicipios');

Route::get('/sedesAll/', 'studentController@mostrarSedes');

Route::get('/programasAll/', 'studentController@mostrarProgramas');

Route::get('/periodosAll/', 'studentController@mostrarPeriodos');

Route::get('/materias/{id}', 'studentController@materias');

Route::get('/buscar', 'Controller@buscar')->name('buscar');

Route::get('/materias','Controller@materias')->name('materias');

Route::resource('docente','docenteController');

Route::resource('calificaciones','calificacionesController');

Route::resource('estructura','estructuraController');

Route::resource('programa','programaController');

Route::resource('periodo','periodoController');

Route::resource('sede','sedeController');

Route::resource('jornada','jornadaController');

Route::resource('curso','cursoController');

Route::resource('administrador','administradorController');

Route::resource('asignacionUnidad','asignacionUnidad');

Route::resource('unidadAprendizaje','unidadAprendizaje');

Route::resource('matricula','matriculaController');


//Route::resource('nota','notasController');

Route::resource('cursoAuxiliar','cursoAuxiliarController');

/* consulta periodos por su id*/
Route::get('/periodoX/{id}','periodoController@mostrarPeriodo');

/* consulta periodos por su id*/
Route::get('/cursosX/{id}','cursoController@indexSede');

/* consulta prueba matricula por su id*/
Route::get('/pruebaMatricula/{oID}/{username}','matriculaController@pruebaGuardar');

/* consulta para ver el resultado de funcion consulta por usrname*/
Route::get('/consultaSuma/{username}','matriculaController@consultaSuma');

/* consulta para ver el resultado de funcion consulta por usrname*/
Route::get('/consultaExiste/{username}/{codigoCurso}/{padre}/{hijo}','matriculaController@consultaSuma');

/* consulta para ver la tabla*/
Route::get('/tabla','matriculaController@vertabla');

/* SE AGREGA DESARROLLO EXERNO */
/*
Route::get('/notass/getAll','NotasController@getAll');
Route::post('/notass/actualizar/{id}','NotasController@updateNote');
Route::get('/notass/crear','NotasController@addNote');
Route::post('/notass/eliminar/{id}','NotasController@deletedNote');*/


/*Route::get('csv',function(){
	return view('csv');
});*/

// estructura de las notas

// estructura de las notas

Route::get('/notas-estructura-ver/{curso_id}', 'calificacionesController@showStructureNotes')->name('notas.estructura.ver');
Route::get('/notas-ver/{curso_id}', 'calificacionesController@showNotes')->name('notas.estructura.ver');
Route::post('/notas-estructura-update/', 'calificacionesController@updateStructureNotes')->name('notas.estructura.actualizar');
Route::post('/notas-estructura-create/', 'calificacionesController@createStructureNotes')->name('notas.estructura.crear');
Route::get('notas', function () {
    return view('notas');
});


