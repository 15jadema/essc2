<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notas extends Model
{
    /**
     *
     * @var string
     */
    protected $table = 'notas';

    /**
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     *
     * 
     * @var array
     */

    protected $fillable = [
        'id',// bigint primary key,
        'codigoGrupo',// bigint,
        'username',// bigint,
        'unidadAprendizaje_id',// bigint,
        'padre',// smallint,
        'porcentajePadre',// integer
        'hijo', //text
        'porcentajeHijo',// integer,
        'nota', //numeric
        'inasistencias',//integer
        'observacion', //text
       // 'idNumero', //bigint
    ];
}
