<?php

namespace App;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
   // public $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      //  'name', 'last_name', 'username', 'email', 'password',
     'username','nombre','apellido','nombre2','apellido2','genero_id','documento_id','fechaNacimiento', 'levelUser_id', 'programa_id','password','direccion','munNacimiento_id', 'sede_id','periodo_id','celular', 'telefono','barrio','status_id'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function docentes(){
        $docente=DB::table('users')
        ->where('levelUser_id', '=',2)       
        ->where('status_id', '=', 2)
      //  ->where('sede_id', '=', $id)    
        ->get();
       // return user::all();
       return $docente;
    }
}
