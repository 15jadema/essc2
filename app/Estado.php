<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    public static function estados(){
        return Estado::all();
    }
}
