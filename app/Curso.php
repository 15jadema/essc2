<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    public static function cursos($id){
        return Curso::where('unidadAprendizaje_id','=',$id)->get();      
    }
}
