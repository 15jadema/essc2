<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class programa extends Model
{
    public static function programas($id)
    {
        return programa::where('sede_id', '=', $id)->get();
    }

    public static function programasOnly()
    {
        $programa = DB::table('programas')
            ->get();
        return $programa;
    }
}
