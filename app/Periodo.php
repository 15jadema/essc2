<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    public static function periodos(){
        $periodos=DB::table('periodos')       
        ->get();      
       return $periodos;
    }
}
