<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Jornada extends Model
{
    public static function jornadas()
    {
        $jornada = DB::table('jornadas')
            ->get();
        return $jornada;
    }
}
