<?php

namespace App\Http\Controllers;

use App\notas;
use DB;
use Illuminate\Http\Request;

/**

 *

 * @tutorial Controller Description: llamar,actualizar y eliminar notas del usuario

 * @author Erwing FC * erwingforerocastro@gmail.com

 * @since 20/02/2020

 */

class notasController extends Controller
{

    public function getGroups($unAp)
    {
        $datos = DB::table('notas as n')->where('unidadAprendizaje_id', $unAp)->select('n.codigoGrupo')->groupBy('n.codigoGrupo')->get();

        return response()->json($datos);
    }

    public function querys($tipo, $group)
    {
        switch ($tipo) {
            case 1:
                return DB::table('notas as n')
                    ->where('n.codigoGrupo', $group)
                    ->select('n.id', 'n.codigoGrupo', 'n.username', 'n.nota', 'n.hijo', 'n.padre', 'n.porcentajeHijo', 'n.unidadAprendizaje_id', 'n.inasistencias', 'n.observacion')
                    ->where('n.hijo', '<>', null)
                    ->orderBy('n.padre', 'ASC')
                    ->orderBy('n.porcentajeHijo', 'ASC')
                    ->get();
                break;
            case 2:
                return DB::table('notas as n')
                    ->where('n.codigoGrupo', $group)
                    ->select('n.id', 'n.codigoGrupo', 'n.username', 'n.hijo', 'n.unidadAprendizaje_id', 'n.inasistencias', 'n.observacion')
                    ->where('n.hijo', '<>', null)
                    ->groupBy('n.id', 'n.codigoGrupo', 'n.username', 'n.hijo', 'n.unidadAprendizaje_id', 'n.inasistencias', 'n.observacion')
                    ->orderBy('n.id', 'ASC')
                    ->get();
                break;
            case 3:
                return DB::table('notas as n')
                    ->where('n.codigoGrupo', $group)
                    ->select('n.padre', 'n.porcentajePadre')
                    ->groupBy('n.padre', 'n.porcentajePadre')
                    ->orderBy('n.padre', 'ASC')->get();
                break;
            case 4:
                return DB::table('notas as n')
                    ->where('n.codigoGrupo', $group)
                    ->where('n.hijo', '<>', null)
                    ->select('n.padre', 'n.hijo', 'n.porcentajeHijo')
                    ->groupBy('n.padre', 'n.hijo', 'n.porcentajeHijo')
                    ->orderBy('n.padre', 'ASC')
                    ->orderBy('n.porcentajeHijo', 'ASC')
                    ->get();
                break;
        }
    }

    public function getAll($group)
    {

        $datos = $this->querys(1, $group);
        //return response()->json($datos);

        $datos2 = $this->querys(2, $group);

        $nestedHeadersPadres = $this->querys(3, $group);

        $nestedHeadersHijos = $this->querys(4, $group);

        /*Cabecera de los datos*/
        $head = array(
            ['title' => 'Nombre(s)', 'type' => 'number', 'width' => '200', 'name' => 'Nombre(s)'],
            ['title' => 'apellido(s)', 'type' => 'number', 'width' => '200', 'name' => 'apellido(s)'],
            ['title' => 'Usuario', 'type' => 'number', 'width' => '100', 'name' => 'Usuario'],
        );

        $tamañoNotas     = [];
        $padre            = '';
        $c                = 1;
        $c2               = 0;
        $c3               = 1;
        $porcentajesHijos = [];

        foreach ($nestedHeadersHijos as $n) {
            $padre  = (blank($padre)) ? $n->padre : $padre;
            $nombre = $n->hijo == '' ? 'N' . $c2 : $n->hijo;
            $head[] = ['title' => $nombre . '-' . $n->porcentajeHijo . ' %', 'type' => 'number', 'width' => '100', 'name' => $nombre . '-' . $n->porcentajeHijo . ' %'];

            if (array_key_exists($c, json_decode(json_encode($nestedHeadersHijos)))) {
                if ($padre != $nestedHeadersHijos[$c]->padre) {
                    $head[]         = ['title' => 'Total' . $c2, 'type' => 'number', 'width' => '100', 'name' => 'Total' . $c2];
                    $tamañoNotas[] = $c3;
                    $c2++;
                    $c3    = 1;
                    $padre = $nestedHeadersHijos[$c]->padre;
                } else {
                    $c3++;
                }
            } else {
                $head[]         = ['title' => 'Total' . $c2, 'type' => 'number', 'width' => '100', 'name' => 'Total' . $c2];
                $tamañoNotas[] = $c3;
            }

            $porcentajesHijos[] = $n->porcentajeHijo;
            $c++;

        }

        array_push($head, ['title' => 'Total P', 'type' => 'number', 'width' => '100', 'name' => 'Total P'], ['title' => 'Concepto', 'type' => 'number', 'width' => '100', 'name' => 'Concepto'], ['title' => 'Inasistencias', 'type' => 'number', 'width' => '100', 'name' => 'Inasistencias'], ['title' => 'Observación', 'type' => 'text', 'width' => '200', 'name' => 'Observación', 'wordWrap' => true]);
        /*fin de la cabecera*/

        /*nestedHeaders*/

        $headSuperior      = array();
        $porcentajesPadres = [];
        $a                 = [['title' => '', 'colspan' => 3]];
        $c                 = 0;

        foreach ($nestedHeadersPadres as $n) {

            array_push($a, ['title' => $n->padre . '-' . $n->porcentajePadre . ' %', 'colspan' => $tamañoNotas[$c] + 1]);
            $porcentajesPadres[] = $n->porcentajePadre;
            $c++;
        }

        array_push($a, ['title' => '', 'colspan' => 4]);

        $headSuperior[0] = $a;

/*     fin de nestedHeaders*/

        $arrayNotas = array();
        $arrayDatos = array();

        foreach ($datos as $dato) {
            if (array_key_exists($dato->username, $arrayNotas)) {
                if (is_array($arrayNotas[$dato->username])) {
                    $a = $arrayNotas[$dato->username];
                } else {
                    $a = array($arrayNotas[$dato->username]);
                }
                array_push($a, $dato->nota . "-" . $dato->porcentajeHijo);
                $arrayNotas[$dato->username] = $a;
            } else {
                $arrayNotas[$dato->username] = $dato->nota . "-" . $dato->porcentajeHijo;
            }
        }

        $c = 1;
        foreach ($datos2 as $dato) {
            $bool = true;
            foreach ($arrayDatos as $value) {
                if (in_array($dato->username, $value)) {
                    $bool = false;
                }
            }

            if ($bool) {
                $nombre = DB::table('users as u')->where('u.username', '=', $dato->username)->select('u.nombre', 'u.nombre2', 'u.apellido', 'u.apellido2')->first();
                if (blank($nombre)) {
                    $arrayDatos[] = ["Sin información", "Sin información", $dato->username];
                } else {
                    $arrayDatos[] = ["$nombre->nombre  $nombre->nombre2", "$nombre->apellido  $nombre->apellido2", $dato->username];
                }

                $keyl = array_key_last($arrayDatos); //posicion del array anterior
                $a    = $arrayDatos[$keyl]; //se guarda ese array
                //array de las notas del usuario
                if (is_array($arrayNotas[$dato->username])) {
                    $b = $arrayNotas[$dato->username];
                } else {
                    $b = [$arrayNotas[$dato->username]];
                }
                $c2          = 0; //contador de la columna final
                $porcentajes = []; //porcentajes de las notas
                $notas       = [];
                $suma        = '=ROUND(';
                $sumav       = 0; //valor de todas las notas (valor ponderado)
                $cnot        = 0;
                foreach ($b as $v) {
                    array_push($a, (float) explode("-", $v)[0]); //concatena la nota
                    $porcentajes[] = ((int) explode("-", $v)[1]) / 100; //concatena los porcentajes
                    $notas[]       = ((float) explode("-", $v)[0]);
                    $tamaño       = ($cnot > 0) ? 2 + (array_sum(array_slice($tamañoNotas, 0, $cnot)) + $c2) : 2; //posicion de las notas ya guardadas

                    // si se coloca la columna de total
                    if ($c == $tamañoNotas[$cnot]) {
                        $A = 'A';
                        //posicion excel columnar [ABCDEF....]
                        for ($i = 1; $i <= $tamaño + 1; $i++) {
                            $A++;
                        }

                        $form = '=ROUND(';
                        $suma .= '+(';
                        $cp           = 0;
                        $sumavInterna = 0;
                        $sumaP        = 0; //si tiene sumatoria de porcentajes de notas
                        //posicion excel filas
                        for ($i = $tamaño; $i < $tamaño + $tamañoNotas[$cnot]; $i++) {
                            $pos = $keyl + 1;
                            if ($porcentajes[$cp] > 0) {
                                $form .= "+" . "$A" . "$pos" . "*" . "$porcentajes[$cp]" . ""; //concatena la formula excel de total
                                $suma .= "+" . "$A" . "$pos" . "*" . "$porcentajes[$cp]" . "";
                                $sumavInterna += $notas[$cp] * $porcentajes[$cp];
                                $sumaP += $porcentajes[$cp] * 100;
                                $cp++;
                                $A++;

                            } else {
                                $form .= "+" . "$A" . "$pos" . "*" . "$porcentajesPadres[$c2]/100" . ""; //concatena la formula excel de total
                                $suma .= "+" . "$A" . "$pos" . "*" . "$porcentajesPadres[$c2]/100" . "";
                                $sumavInterna += $notas[$cp] * $porcentajesPadres[$c2] / 100;
                                $sumaP += $porcentajesPadres[$c2];
                                $cp++;
                                $A++;

                            }

                        }
                        $sumavInterna = round($sumavInterna, 2);
                        $form .= ', 2)';
                        $suma .= ($sumaP >= 100) ? ")*" . "$porcentajesPadres[$c2]/100" : ")";
                        $sumav += ($sumaP >= 100) ? $sumavInterna * $porcentajesPadres[$c2] / 100 : $sumavInterna;
                        array_push($a, $form); //se concatena la formula total
                        $porcentajes = []; //se reinicia el porcentaje de notas
                        $notas       = []; //se reinicia el guardado de notas
                        $c           = 1; //se reinicia el contador de notas
                        $c2++; //aumenta segun el numero de columnas total ya colocadas
                        $cnot++; //aumenta la posicion del tamaño de las notas
                    } else {
                        $c++;
                    }
                }

                $suma .= ', 2)';
                // se llena con los valores siguientes
                $valoresFaltantes = [$suma, ($sumav >= 3.5) ? "Aprovado" : "Desaprovado", $dato->inasistencias, $dato->observacion];
                foreach ($valoresFaltantes as $v) {
                    array_push($a, $v);
                }
                //se cambia ese array por el anterior ya llenado con todos los valores
                $arrayDatos[$keyl] = $a;
            }
        }

        $arrayAux = [];
        $c        = 0;
        foreach ($arrayDatos as $a) {
            $arr = [];
            for ($i = 0; $i < count($head); $i++) {
                $arr[$c][$head[$i]['title']] = $a[$i];
            }
            $arrayAux[] = $arr[$c];
            $c++;
        }
        $arrayDatos = $arrayAux;
        return response()->json([$arrayDatos, $head, $headSuperior[0]]);
    }

    public function updateNote($user, $codg, Request $request)
    {
        $notaSend           = $request->valores;
        $nestedHeadersHijos = DB::table('notas as n')
            ->where('n.codigoGrupo', $codg)
            ->where('n.hijo', '<>', null)
            ->select('n.padre', 'n.porcentajePadre', 'n.hijo', 'n.porcentajeHijo')
            ->groupBy('n.padre', 'n.porcentajePadre', 'n.hijo', 'n.porcentajeHijo')
            ->orderBy('n.padre', 'ASC')
            ->orderBy('n.porcentajeHijo', 'ASC')->get();
        $nestedHeadersPadres = $this->querys(3, $codg);

        $tamañoNotas = [];
        $padre        = '';
        $c            = 1;
        $c3           = 1;
        return response()->json([$nestedHeadersHijos, $nestedHeadersPadres]);
        foreach ($nestedHeadersHijos as $n) {
            $padre = (blank($padre)) ? $n->padre : $padre;

            if (array_key_exists($c, json_decode(json_encode($nestedHeadersHijos)))) {
                if ($padre != $nestedHeadersHijos[$c]->padre) {
                    $tamañoNotas[] = $c3;
                    $c3             = 1;
                    $padre          = $nestedHeadersHijos[$c]->padre;
                } else {
                    $c3++;
                }
            } else {
                $tamañoNotas[] = $c3;
            }
            $c++;

        }
        $uv  = array_key_last($notaSend); //ultima key del array
        $pos = 3; //posicion de la primera nota
        $c   = 1; //contador de totales
        $cn  = 0; //contador del tamaño de notas
        for ($i = 0; $i < count($nestedHeadersHijos); $i++) {
            if ($c == $tamañoNotas[$cn] + 1) {
                $c = 1;
                $cn++;
                $i--;
            } else {

                DB::table('notas')->where('username', $user)->where('codigoGrupo', $codg)
                    ->where('padre', $nestedHeadersHijos[$i]->padre)
                    ->where('porcentajePadre', $nestedHeadersHijos[$i]->porcentajePadre)
                    ->where('hijo', $nestedHeadersHijos[$i]->hijo)
                    ->where('porcentajeHijo', $nestedHeadersHijos[$i]->porcentajeHijo)
                    ->update(['nota' => (float) $notaSend[$pos],
                        'inasistencias'  => $notaSend[$uv - 1],
                        'observacion'    => $notaSend[$uv],
                    ]);
                $c++;
            }
            $pos++;

        }

    }
    public function deletedNote($user, $codg)
    {
        try {
            DB::table('notas as n')->where('username', $user)->where('codigoGrupo', $codg)->delete();
        } catch (Exception $e) {
            return "Excepcion encontrada $e";
        }

    }

}
