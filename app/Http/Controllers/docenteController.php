<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\departamento;
use App\municipios;
use App\documento;
use App\genero;
use App\Sede;
use DB;
use Illuminate\Support\Facades\Hash;

use  App\Http\Requests\DocenteStoreRequest;
use  App\Http\Requests\DocenteUpdateRequest;

class docenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docente = DB::table('users')->where('levelUser_id', '=', 2)->orderBy('apellido', 'asc')->paginate(10);
        // $docente = User::where('levelUser_id',2);
        return view('docente.index', compact('docente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genero = genero::all();
        $documento = documento::all();
        $departamento = departamento::all();
        $sede = Sede::all();
        return view('docente.create', compact('departamento', 'documento', 'genero', 'sede'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocenteStoreRequest $request)
    {
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            //return $name;
        } else {
            $name = "sinfoto.jpg";
        }
        $e = new User();
        $e->username = $request->username;
        $e->apellido = $request->apellido;
        $e->apellido2 = $request->apellido2;
        $e->nombre = $request->nombre;
        $e->nombre2 = $request->nombre2;
        $e->genero_id = $request->genero_id;
        $e->documento_id = $request->documento_id;
        $e->fechaNacimiento = $request->fechaNacimiento;
        $e->depaNacimiento_id = $request->depaNacimiento_id;
        $e->munNacimiento_id = $request->munNacimiento_id;
        $e->telefono = $request->telefono;
        $e->celular = $request->celular;
        $e->email = $request->email;
        $e->direccion = $request->direccion;
        $e->tituloAcademico = $request->tituloAcademico;
        $e->foto = $name;
        $e->sede_id = $request->sede_id;
        $e->status_id = 2;
        $e->levelUser_id = $request->levelUser_id;       
        $e->password = Hash::make($request->username);

        $e->save();
        return redirect('/docente/')->with('success', 'Docente: '.$request->nombre.' '.$request->apellido. ', registrado satisfactoriamente.');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $docente=DB::table('users')        
        ->where('users.id', '=', $id)   
        ->join('generos', 'users.genero_id', '=', 'generos.id')  
        ->get();
        return view('docente.show', compact('docente','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $docente)
    {
       return view('docente.edit', compact('docente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function update(DocenteUpdateRequest $request, User $docente)
    {
        $docente->fill($request->all());
        $docente->save();
        return redirect('/docente')->with('success', 'Docente: '.$docente->username.' --- '.$request->nombre.' '.$request->apellido. ', actualizado satisfactoriamente');
       // return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function docenteCurso($id)
    {
        $docente=DB::table('users')
        ->where('levelUser_id', '=',2)       
        ->where('status_id', '=', 1)
        ->where('sede_id', '=', $id)    
        ->get();
        return view('docente.show', compact('docente','id'));
    }

    public function mostrarDocentes(Request $request, $id)
    {
       if ($request->ajax()) {
            $docentes = User::docentes($id);
            return response()->json($docentes);
        }
       
    }
}
