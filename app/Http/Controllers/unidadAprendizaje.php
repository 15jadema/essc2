<?php

namespace App\Http\Controllers;

use App\UnidadAprendizaje as AppUnidadAprendizaje;
use Illuminate\Http\Request;
use DB;

class unidadAprendizaje extends Controller
{
  public function index(Request $request)
  {
    $student = $request->username;
    $unidades = DB::table('asignar_unidads')
      ->join('unidad_aprendizajes', 'asignar_unidads.id', '=', 'unidad_aprendizajes.id')
      ->join('nivels', 'asignar_unidads.nivel', '=', 'nivels.id')
      ->where('asignar_unidads.programa_id', '=', 1)
      ->orderBy('nivel')
      ->get();
    return view('unidadAprendizaje.index', compact('unidades', 'student'));
  }

  public function mostrarUnidades(Request $request, $id)
  {
    if ($request->ajax()) {

      $unidades = AppUnidadAprendizaje::unidades($id);
      return response()->json($unidades);
    }
  }
}
