<?php

namespace App\Http\Controllers;

use App\Periodo;
use App\estado;
use Illuminate\Http\Request;
use DB;

class periodoController extends Controller
{
  public function create()
  {
    $estado = estado::all();
    return view('periodo.create', compact('estado'));
  }

  public function store(Request $request)
  {
    $p = new Periodo();
    $p->nomPeriodo = $request->nomPeriodo;
    $p->ordenamiento = $request->ordenamiento;
    $p->estado_id = $request->estado_id;
    $p->fechaInicio = $request->fechaInicio;
    $p->fechaFin = $request->fechaFin;
    $p->save();
    return redirect('/periodo/');
  }

  public function index()
  {
    $periodo = DB::table('periodos')->paginate(10);
    return view('periodo.index', compact('periodo'));
  }


  public function mostrarPeriodo($id)
  {
   $periodo = DB::table('periodos')->where('id', '=', $id)->get();
    return response()->json($periodo);   
  }
}
