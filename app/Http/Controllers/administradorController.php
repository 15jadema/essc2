<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\departamento;
use App\municipios;
use App\documento;
use App\genero;
use App\sede;

class administradorController extends Controller
{

    public function create()
    {
        $genero = genero::all();
        $documento = documento::all();
        $departamento = departamento::all();
        $sede = sede::all();
        return view('administrador.create', compact('departamento', 'documento', 'genero', 'sede'));
    }

    public function show($id)
    {
        $administrador = User::find($id);
        return view('administrador.show', compact('administrador'));
    }
    public function store(Request $request)
    {
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
        } else {

            $name = "sinfoto.jpg";
        }
        $e = new User();
        $e->username = $request->username;
        $e->apellidos = $request->apellido;
        $e->apellidos2 = $request->apellido2;
        $e->nombres = $request->nombre;
        $e->nombres2 = $request->nombre2;
        $e->genero_id = $request->genero_id;
        $e->documento_id = $request->documento_id;
        $e->fechaNacimiento = $request->fechaNacimiento;
        $e->depaNacimiento_id = $request->depaNacimiento_id;
        $e->munNacimiento_id = $request->munNacimiento_id;
        $e->foto = $name;
        $e->levelUser_id = $request->levelUser_id;
        $e->password = Hash::make($request->username);
        $e->save();
        return redirect('/administrador/');
    }

    public function index()
    {
        $administrador = DB::table('users')->where('levelUser_id', '=', 1)->get();
        return view('administrador.index', compact('administrador'));
    }
}
