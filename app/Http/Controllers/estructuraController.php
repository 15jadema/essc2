<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use DB;
use App\estado;
use App\UnidadAprendizaje;

class estructuraController extends Controller
{
    public function create()
    {
      $estado = estado::all();
    
      return view('estructura.create', compact('estado'));
    }

    public function store(Request $request)
    {
        $e = new UnidadAprendizaje();
        $e->codigo = $request->codigo;
        $e->nomUnidadAprendizaje = $request->nomUnidadAprendizaje;
        $e->abreviatura = $request->abreviatura;
        $e->estado_id = $request->estado_id;
        $e->usarComoPractica = $request->usarComoPractica;
        $e->tipoEvaluacion = $request->tipoEvaluacion;
        $e->save();
        return redirect('/student/');
    }

    public function index()
    {
        $estructura = DB::table('unidad_aprendizajes')->paginate(10);
        return view('estructura.index', compact('estructura'));
    }
}
