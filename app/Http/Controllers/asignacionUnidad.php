<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\programa;
use App\UnidadAprendizaje;
use App\nivel;
use App\asignarUnidad;
use Illuminate\Http\Request\Input;

class asignacionUnidad extends Controller
{
    public function index()
    {
        $unidadAprendizaje = UnidadAprendizaje::all();
        return view('asignacionUnidad.index', compact('unidadAprendizaje'));
    }

    public function show($id)
    {
        //  $student="2743542";        
        $programa = DB::table('programas')->where('id', $id)->first();
        //  $unidadAprendizaje =UnidadAprendizaje::all(); 
        $asignatura = DB::table('asignar_unidads')
            ->join('unidad_aprendizajes', 'asignar_unidads.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
            ->join('nivels','asignar_unidads.nivel','=','nivels.id')
            ->where('asignar_unidads.programa_id', '=', $id)
            ->get();

        // return view('asignacionUnidad.show', compact('programa','asignatura','unidadAprendizaje','student'));
        return view('asignacionUnidad.show', compact('programa', 'asignatura'));
    }

    public function create(Request $request)
    {
        $miPrograma = $request->input('miPrograma');
        $nivel = nivel::all();
        $unidadAprendizaje = UnidadAprendizaje::all();
        return view('asignacionUnidad.create', compact('unidadAprendizaje', 'nivel', 'miPrograma'));
    }

    public function store(Request $request)
    {
        $asi = new asignarUnidad();
        $idPrograma=$request->programa_id;
        $asi->unidadAprendizaje_id = $request->unidadAprendizaje_id;
        $asi->programa_id = $request->programa_id;
        $asi->nivel = $request->nivel;
        $asi->creditos = $request->creditos;
        $asi->ihs = $request->ihs;
        $asi->iht = $request->iht;
        $asi->cerrar = $request->cerrar;
        $asi->save();
        return redirect('/asignacionUnidad/'.$idPrograma);
    }
}
