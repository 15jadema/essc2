<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use DB;
use App\User;
use App\departamento;
use App\municipios;
use App\documento;
use App\genero;
use App\Sede;
use App\programa;
use App\Periodo;
use App\Nota;
use App\matricula;
use App\Curso;
use App\Estado;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use  App\Http\Requests\StudentStoreRequest;
use  App\Http\Requests\StudentUpdateRequest;


class studentController extends Controller
{
    public function create()
    {
        $genero = genero::all();
        $documento = documento::all();
        $departamento = departamento::all();
        $sede = DB::table('sedes')->get();
        $programa = programa::all();
        $periodo = DB::table('periodos')->orderBy('periodos.id', 'desc')->get();
        $municipios = municipios::all();

        return view('student.create', compact('departamento', 'documento', 'genero', 'sede', 'programa', 'periodo', 'municipios'));
    }

    public function show($id)
    {
        $student = DB::table('users')
            ->where('users.id', '=', $id)
            ->join('generos', 'users.genero_id', '=', 'generos.id')
            ->join('programas', 'users.programa_id', '=', 'programas.id')
            ->get();

        $asignatura = DB::table('asignar_unidads')
            ->join('unidad_aprendizajes', 'asignar_unidads.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
            ->join("users", "asignar_unidads.programa_id", "=", "users.programa_id")
            ->where('users.id', '=', $id)
            ->orderBy('nivel', 'asc')
            ->get();
        return view('student.show', compact('student', 'asignatura', 'id'));
    }

    public function index()
    {
        $student = DB::table('users')
            ->join('programas', 'users.programa_id', '=', 'programas.id')
            ->where('levelUser_id', '=', 3)
            ->Where('username', '!=', '0')
            ->orderBy('apellido', 'asc')
            ->orderBy('apellido2', 'asc')
            ->get(array('programas.abreviatura', 'users.levelUser_id', 'users.status_id', 'users.celular', 'users.telefono', 'users.username', 'users.nombre', 'users.nombre2', 'users.apellido', 'users.apellido2', 'users.id'));
        return view('student.index', compact('student'));
    }

    public function mostrar(Request $request, $id)
    {
        if ($request->ajax()) {
            $municipios = municipios::municipios($id);
            return response()->json($municipios);
        }
    }


    public function mostrarDoc(Request $request, $id)
    {
        if ($request->ajax()) {
            $documentos = documento::documentos($id);
            return response()->json($documentos);
        }
    }

    public function mostrarPro(Request $request, $id)
    {
        if ($request->ajax()) {
            $programas = programa::programas($id);
            return response()->json($programas);
        }
    }

    public function mostrarstudentdecurso(Request $request, $id)
    {
        if ($request->ajax()) {
            /*  $estudiantes = DB::table('matriculas')
                ->join('users', 'matriculas.username', '=', 'users.username')
                ->join('cursos', 'matriculas.codigo_id', '=', 'cursos.id')
                ->where('matriculas.codigo_id', '=', $id)
                ->orderBy('users.apellido', 'asc')
                ->get();*/
            $parametros = DB::table('p_notas')->get();
            $estudiantes = DB::table('notas')
                /*->leftJoin('matriculas','notas.codigoGrupo','=','matriculas.codigo_id')*/
                ->join('users', 'notas.username', '=', 'users.username')
                ->join('cursos', 'notas.codigoGrupo', '=', 'cursos.id')
                ->join('p_notas', 'cursos.id', '=', 'p_notas.curso_id')
                //->where('p_notas.id','=',3)
                ->where('notas.codigoGrupo', '=', $id)
                //  ->where('notas.idNumero', '=', 3)
                ->orderBy('users.apellido', 'asc')
                ->get(array('notas.id', 'notas.codigoGrupo', 'notas.username', 'notas.unidadAprendizaje_id', 'notas.padre', 'notas.porcentajePadre', 'notas.hijo', 'notas.porcentajeHijo', 'notas.nota', 'notas.inasistencias', 'notas.observacion', 'users.apellido', 'users.apellido2', 'users.nombre', 'users.nombre2'));
            return view('cursoAuxiliar.detallecurso', compact('estudiantes', 'parametros'));
        }/* aqui se debe modificar para que busque en las notas */
    }

    public function consultaExiste($username, $oID)
    {
        return DB::table('notas')
            ->select('notas.username')
            ->where('notas.username', '=', $username)
            ->where('notas.codigo_id', '=', $oID)
            ->where('notas.padre', '=', $oID)
            ->where('notas.hijo', '=', $oID)
            ->count();
    }

    public function mostrarnotascurso(Request $request, $id)
    {
        if ($request->ajax()) {
            $porciones = explode("-", $id);
            $idUsu = $porciones[0]; // porción1
            $idPer = $porciones[1]; // porción2

            /*
    $teams = DB::table('teams')
    ->select(DB::raw('id, name, (SELECT COUNT(id_team_local) FROM seasons WHERE id_team_local = teams.id) AS 'Partidos jugados'))
    ->get();
    */

            $notas = DB::table('notas')
                //  ->select(DB::notas('username,(SELECT * FROM notas WHERE unidadAprendizaje_id = 13, codigoGrupo=1)'))
                // ->rightJoin('matriculas', 'notas.codigoGrupo', '=', 'matriculas.codigo_id')
                ->join('unidad_aprendizajes', 'notas.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
                ->join('cursos', 'notas.codigoGrupo', '=', 'cursos.id')
                ->join('users', 'cursos.docente_id', '=', 'users.username')
                ->join('periodos', 'cursos.periodo_id', '=', 'periodos.id')
                //  ->join('unidad_aprendizajes','cursos.unidadAprendizaje_id','=','unidad_aprendizajes.id') 
                ->where('notas.username', '=', $idUsu)
                //  ->where('cursos.unidadAprendizaje_id','=','unidad_aprendizajes.id')
                //  ->where('cursos.periodo_id', '=', $idPer)
                //->groupBy('notas.periodo_id')
                //->orderBy('users.apellido','asc')
                ->orderBy('notas.codigoGrupo', 'asc')
                ->orderBy('notas.unidadAprendizaje_id', 'asc')
                ->orderBy('notas.padre', 'asc')
                ->get();
            return view('cursoAuxiliar.detallenotas', compact('notas', $notas));
        }
        /*
        if ($request->ajax()) {
        $estudiantes = DB::table('matriculas')
        ->join('users', 'matriculas.username', '=', 'users.username')
        ->where('matriculas.codigo_id', '=', $id)
        ->orderBy('users.apellido','asc')
        ->get();
            return view('cursoAuxiliar.detallenotas', compact('estudiantes', $estudiantes));
        }*/
    }


    public function mostrarCurso(Request $request, $id)
    {
        if ($request->ajax()) {
            $cursos = Curso::cursos($id);
            return response()->json($cursos);
        }
    }

    public function mostrarNota(Request $request, $id)
    {
        if ($request->ajax()) {
            $notas = Nota::notas($id);
            return response()->json($notas);
        }
    }

    public function store(StudentStoreRequest $request)
    {
        $e = new User();
        $v = Validator::make($request->all(), [
            'username' => 'required',
            'apellido' => 'required',
            'email'    => 'required|email|unique:User'
        ]);

        $e->username = $request->username;
        $e->apellido = $request->apellido;
        $e->apellido2 = $request->apellido2;
        $e->nombre = $request->nombre;
        $e->nombre2 = $request->nombre2;
        $e->genero_id = $request->genero_id;
        $e->email = $request->email;
        $e->telefono = $request->telefono;
        $e->celular = $request->celular;
        $e->documento_id = $request->documento_id;
        $e->fechaNacimiento = $request->fechaNacimiento;
        $e->depaNacimiento_id = $request->depaNacimiento_id;
        $e->munNacimiento_id = $request->munNacimiento_id;
        $e->sede_id = $request->sede_id;
        $e->programa_id = $request->programa_id;
        $e->foto = "sinfoto.jpg";
        $e->levelUser_id = $request->levelUser_id;
        $e->password = Hash::make($request->username);
        $e->barrio = $request->barrio;
        $e->periodo_id = $request->periodo_id;
        $e->direccion = $request->direccion;
        $e->status_id = 2;
        $e->save();
        return redirect('/student/')->with('success', 'Estudiante: ' . $request->nombre . ' ' . $request->apellido . '  ,registrado satisfactoriamente');
    }


    public function edit(User $student)
    {
        $documento = documento::all();
        return view('student.edit', compact('student', 'documento'));
    }

    public function update(StudentUpdateRequest $request, User $student)
    {
        $student->fill($request->all());
        $student->save();
        return redirect('/student')->with('success', 'Estudiante: ' . $student->username . ' --- ' . $request->nombre . ' ' . $request->apellido . ', actualizado satisfactoriamente');
    }

    public function mostrarGenero(Request $request, $id)
    {
        if ($request->ajax()) {
            $generos = genero::generos($id);
            return response()->json($generos);
        }
    }

    public function mostrarEstado(Request $request, $id)
    {
        if ($request->ajax()) {
            $estados = Estado::estados($id);
            return response()->json($estados);
        }
    }

    public function mostrarMunicipios()
    {
        $municipios = municipios::all();
        return response()->json($municipios);
    }

    public function mostrarSedes()
    {
        $sedes = Sede::all();
        return response()->json($sedes);
    }

    public function mostrarProgramas()
    {
        $programas = programa::all();
        return response()->json($programas);
    }

    public function mostrarPeriodos()
    {
        $periodos = periodo::all();
        return response()->json($periodos);
    }

    public function buscar(Request $request)
    {
        $bus = trim($request->search);
        $student = User::where('nombre', 'like', '%' . $bus . '%')
            ->orWhere('nombre2', 'like', '%' . $bus . '%')
            ->orWhere('apellido', 'like', '%' . $bus . '%')
            ->orWhere('apellido2', 'like', '%' . $bus . '%')
            ->orderBy('apellido', 'asc')
            ->orderBy('apellido2', 'asc')
            ->get();
        return view('student.index', compact('student'));
    }

    public function updateEstado($id, $idEstado)
    {
        $usuario = User::findOrFail("id");
        $usuario->status_id = $idEstado;
        $usuario->update();
        return back();
    }
}
