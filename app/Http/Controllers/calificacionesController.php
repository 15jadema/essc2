<?php
namespace App\Http\Controllers;

use App\estructura_nota;
use DB;
use Illuminate\Http\Request;
use matricula;
use notas;

class calificacionesController extends Controller
{
    public function index()
    {
        $sede   = DB::table('sedes')->where('id', '=', 1)->get();
        $unidad = DB::table('unidad_aprendizajes')->get();
        return view('calificaciones.index', compact('sede', 'unidad'));
        /*
        ->join('unidad_aprendizajes', 'asignar_unidads.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
        ->where('asignar_unidads.programa_id', '=', $id)*/
/*
$listaMatriculados = DB::table('matricula')
->join('cursos','matricula.curso_id','=','cursos.id')
->join('unidadAprendizaje','cursos.unidadAprendizaje_id','=','cursos.id')
->where('unidadAprendizaje','=',1)->get();*/
    }

    public function mostrarUnidadAprendizaje(Request $request, $id)
    {

        if ($request->ajax()) {
            // $municipios = municipios::municipios($id);
            //  return response()->json($municipios);
        }
    }

    public function createStructureNotes(Request $request)
    {

        try {
            //consultas
            $query      = DB::table('estructura_notas as es')->where('es.id_curso', $request->curso_id);
            $orden      = $query->count();
            $porcentaje = $query->whereNull('id_padre')->select(DB::raw('SUM(es.porcentaje_nota) as porcentaje_total'))->first();
            $porcentaje = blank($porcentaje) ? 0 : $porcentaje->porcentaje_total;

            //creación de esquema
            $est_nota                         = new estructura_nota();
            $est_nota->nombre_nota            = $request->nombreNota;
            $est_nota->id_unidadDeAprendizaje = $request->unidad_id;
            $est_nota->id_curso               = $request->curso_id;
            $est_nota->orden                  = (blank($orden)) ? 1 : $orden + 1;
            $est_nota->porcentaje_nota        = $request->porcentajeNota;
            $est_nota->cantidad_subnotas      = $request->cantidadNotas;
            $est_nota->save();

            if ($request->cantidadNotas > 0) {

                $arrayNotasName = $request->subNotasName;
                $arrayNotasPorc = $request->subNotasPorc;

                for ($i = 0; $i < count($arrayNotasName); $i++) {
                    $est                  = new estructura_nota();
                    $est->nombre_nota     = $arrayNotasName[$i];
                    $est->porcentaje_nota = (blank($arrayNotasPorc[$i])) ? 0 : $arrayNotasPorc[$i];
                    $est->id_padre        = $est_nota->id;
                    $est->save();
                }

            }
            //inserción de usuarios en notas
            if ($porcentaje + $request->porcentajeNota == 100) {
                $this->insertUsersNotes($request->curso_id);
            }

        } catch (Exception $e) {

            return 'Excepción capturada:' . $e->getMessage();
        }

    }

    public function insertUsersNotes($curso_id): void
    {
        $usuarios = matricula::where('codigo_id', $curso_id)->get()->toArray();
        $subQuery = DB::table('estructura_notas as es')
            ->where('es.id_curso', $curso_id)
            ->join('estructura_notas as es2', 'es.id', '=', 'es2.id_padre')
            ->orderBy('es.orden')
            ->select('es.orden', 'es.nombre_nota as n1', 'es.porcentaje_nota as p1',
                'es2.nombre_nota as n2', 'es2.porcentaje_nota as p2')->get();

        foreach ($usuarios as $el) {
            $query      = notas::where('codigoGrupo', $curso_id)->where('username', $el->username)->first()->isEmpty();
            $estructura = [];
            if ($query) {
                foreach ($subQuery as $value) {
                    $nota = ['codigoGrupo' => $curso_id, 'username'         => $el->username,
                        'padre'                => $value->n1, 'porcentajePadre' => $value->p1,
                        'hijo'                 => $value->n2, 'porcentajeHijo'  => $value->p2, 'nota' => 0,
                        'inasistencias'        => 0, 'observacion'              => "", 'idNumero'     => 0];
                    array_push($estructura, $nota);
                }
                foreach ($estructura as $value) {
                    notas::create($value);
                }

            } else {
                foreach ($subQuery as $value) {
                    $nota = ['padre' => $value->n1, 'porcentajePadre' => $value->p1,
                        'hijo'           => $value->n2, 'porcentajeHijo'  => $value->p2];
                    array_push($estructura, $nota);
                }
                foreach ($estructura as $value) {
                    notas::where('codigoGrupo', $curso_id)->where('padre', $value['padre'])->where('hijo', $value['hijo'])
                        ->update(['porcentajePadre' => $value['porcentajePadre'],
                            'porcentajeHijo'            => $value['porcentajeHijo']]);
                }
            }
        }

    }

    public function showStructureNotes($curso_id)
    {
        try {

            $est             = estructura_nota::where('id_curso', $curso_id)->orderBy('orden', 'ASC')->get();
            $respuesta       = 'Estructura no creada';
            $estructuraFinal = [];

            if (!blank($est)) {

                foreach ($est as $e) {
                    $hijos             = estructura_nota::where('id_padre', $e->id)->get();
                    $estructuraFinal[] = [$e->id, $e->nombre_nota, $e->porcentaje_nota, 'hijos' => $hijos];
                }

                $respuesta = $estructuraFinal;

            }

            return response()->json([
                'html' => view('calificaciones.parts.tabla_estructura')->with([
                    'respuesta' => $respuesta])
                    ->render(),
            ]);

        } catch (Exception $e) {
            return 'Excepción capturada:' . $e->getMessage();
        }

    }
    public function showNotes($curso_id)
    {
        try {
            return response()->json([
                'html' => view('notas.parts.estructura_notas')->with(['curso_id' => $curso_id])->render(),
            ]);

        } catch (Exception $e) {

            return 'Excepción capturada:' . $e->getMessage();
        }
    }

    public function updateStructureNotes(Request $request)
    {
        try {
            $tipo = $request->tipo;
            $est  = estructura_nota::find($request->id)->update([
                "$tipo" => $request->valor,
            ]);
            $this->updateStructureNotes($est->id_curso);

        } catch (Exception $e) {

            return 'Excepción capturada:' . $e->getMessage();
        }

    }

}
