<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Nota;

class registroController extends Controller
{
  public function guardar(Request $request)
  {   

    $respueta = DB::table('notas')
      ->select('notas.id')
      ->where('notas.username', '=', $request->username)
      ->where('notas.codigoGrupo', '=', $request->codigoGrupo)
      ->where('notas.porcentajePadre', '=', $request->porcentajePadre)
      ->where('notas.porcentajeHijo', '=', $request->porcentajeHijo)
      ->count();

    if ($respueta <= 0) {
      $e = new Nota();
      $e->codigoGrupo = $request->codigoGrupo;
      $e->username = $request->username;
      $e->unidadAprendizaje_id = $request->unidadAprendizaje_id;
      $e->padre = $request->padre;
      $e->porcentajePadre = $request->porcentajePadre;
      $e->hijo = $request->hijo;
      $e->porcentajeHijo = $request->porcentajeHijo;
      $e->nota = $request->nota;
      $e->inasistencias = $request->inasistencia;
      $e->observacion = $request->observacion;
      $e->save();
    } else {
      //actualiza
      DB::table('notas')
        ->where('notas.id', $request->id)
        ->update(['nota' => $request->nota, 'observacion' => $request->observacion]);
    }
  }
}
