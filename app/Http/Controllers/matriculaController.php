<?php

namespace App\Http\Controllers;

use App\matricula;
use DB;
use Illuminate\Http\Request;

class matriculaController extends Controller
{
    public function index(Request $request)
    {
        $student = $request->username;
        $id      = $request->codigo_id;
        $cursos  = DB::table('cursos')->where('unidadAprendizaje_id', '=', $id)
            ->join('users', 'users.id', '=', 'cursos.docente_id')
            ->get();
        return view('matricula.index', compact('cursos', 'student'));
    }

    public function store(Request $request)
    {
        $mat            = new matricula();
        $mat->username  = $request->username;
        $mat->codigo_id = $request->codigo_id;
        $mat->estado_id = 1;
        $now            = new \DateTime();
        $now->format('Y-m-d');
        $mat->fechaMatricula = $now;
        $mat->save();
        return redirect('/matricula/');
    }

    public function pruebaGuardar($oID, $username)
    {
        $resultado = self::consultaSuma($username, $oID);
        if ($resultado > 0) {
            //return "existe registro, no se podria guardar";
            return 1;
        } else {
            $fechaActual = date('Y-m-d');
            DB::table('matriculas')->insert([
                'username'       => $username,
                'codigo_id'      => $oID,
                'estado_id'      => 1,
                'fechaMatricula' => $fechaActual,
            ]);

            /* DB::table('notas')->insert([
            'codigoGrupo' => $oID,
            'username' => $username,
            'unidadAprendizaje_id' => 5,
            'padre' => 'sin Padre',
            'porcentajePadre' => 100
            ]);*/
            return 0;
        }
    }

    public function consultaSuma($username, $oID)
    {
        return DB::table('matriculas')
            ->select('matriculas.username')
            ->where('matriculas.username', '=', $username)
            ->where('matriculas.codigo_id', '=', $oID)
            ->count();
    }

    public function show($id)
    {
        $datosCurso = DB::table('cursos')
            ->join('users', 'cursos.docente_id', '=', 'users.username')
            ->join('periodos', 'cursos.periodo_id', '=', 'periodos.id')
            ->join('unidad_aprendizajes', 'cursos.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
            ->where('cursos.id', '=', $id)->get();
        /**hacer la consulta inicial para ver si ya esta matriculado */

        $matriculados = DB::table('matriculas')
            ->join('users', 'matriculas.username', '=', 'users.username')
            ->where('codigo_id', '=', $id)
            ->orderBy('users.apellido', 'asc')
            ->orderBy('users.nombre', 'asc')
            ->get();

        /* $student = DB::table('users')
        ->where('users.id', '=', $id)
        ->join('generos', 'users.genero_id', '=', 'generos.id')
        ->join('programas','users.programa_id','=','programas.id')
        ->get();
        $periodo = Periodo::all();

        $asignatura = DB::table('asignar_unidads')
        ->join('unidad_aprendizajes', 'asignar_unidads.unidadAprendizaje_id', '=', 'unidad_aprendizajes.id')
        ->join("users","asignar_unidads.programa_id","=","users.programa_id")
        //->where('asignar_unidads.programa_id', '=', 5)
        ->where('users.id', '=', $id)
        ->orderBy('nivel', 'asc')
        ->get();
        return view('student.show', compact('student', 'asignatura', 'periodo', 'id'));
         */
        return view('matricula.show', compact('matriculados', 'datosCurso'));
    }

    public function vertabla()
    {
        // $sexo =  DB::table('generos')->get();, compact('sexo')
        /*
        $fecha_nacimiento  = "1964-12-10";
        $edad = \Carbon\Carbon::parse($fecha_nacimiento)->age;*/
        $username    = 1085336644;
        $oID         = 1553;
        $fechaActual = date('Y-m-d');
        /*  $mat = new matricula();
        $mat->username = $username;
        $mat->codigo_id = $oID;
        $mat->estado_id = 1;
        $mat->fechaMatricula = $fechaActual;
        $mat->save();*/

        DB::table('matriculas')->insert([
            'username'       => $username,
            'codigo_id'      => $oID,
            'estado_id'      => 1,
            'fechaMatricula' => $fechaActual,
        ]);

        return view('tabla.index');
    }
}
