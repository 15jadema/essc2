<?php

namespace App\Http\Controllers;

use App\estado;
use App\Jornada;
use Illuminate\Http\Request;

class sedeController extends Controller
{
    public function create()
    {
      $estado = estado::all();
     // $sede = Sede::all();
  
      return view('sede.create', compact('estado'));
    }


    public function store(Request $request)
    {
        $e = new Sede();
        $e->nom = $request->codigo;
        $e->nomUnidadAprendizaje = $request->nomUnidadAprendizaje;
        $e->abreviatura = $request->abreviatura;
        $e->estado_id = $request->estado_id;
        $e->usarComoPractica = $request->usarComoPractica;
        $e->tipoEvaluacion = $request->tipoEvaluacion;
        $e->save();
        return redirect('/student/');
    }
}
