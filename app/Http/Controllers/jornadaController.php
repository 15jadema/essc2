<?php

namespace App\Http\Controllers;
use DB;
use App\estado;
use App\Jornada;
use Illuminate\Http\Request;

class jornadaController extends Controller
{
    public function create()
    {
      $estado = estado::all();
   
      return view('jornada.create', compact('estado'));
    }

    public function store(Request $request)
    {
        $e = new Jornada();
        $e->nomJornada = $request->nomJornada;
        $e->estado_id = $request->estado_id;
        $e->save();
        return redirect('/student/');
    }

    public function mostrarJornadas(Request $request, $id)
    {
      if ($request->ajax()) {  
        $jornada = Jornada::jornadas($id);
        return response()->json($jornada);
      }
    }
}
