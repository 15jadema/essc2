<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function buscar(Request $request)
    {
        $bus = trim($request->search);
        $student = DB::table('users')
            ->join('programas', 'users.programa_id', '=', 'programas.id')
            ->where('nombre', 'like', '%' . $bus . '%')
            ->orWhere('nombre2', 'like', '%' . $bus . '%')
            ->orWhere('apellido', 'like', '%' . $bus . '%')
            ->orWhere('username', '=', $bus)
            ->Where('username', '!=', '0')
            ->orderBy('apellido', 'asc')
            ->orderBy('apellido2', 'asc')
            ->get(array('programas.abreviatura', 'users.levelUser_id','users.status_id' ,'users.celular', 'users.telefono', 'users.username','users.nombre','users.nombre2', 'users.apellido', 'users.apellido2','users.id'));
        return view('student.index', compact('student'));
        /*
        $bus=$request->search;
        $student = User::where('levelUser_id',1)
        ->Where('nombre', 'like', '%'.$bus.'%')
        ->orWhere('username', '=', $bus)
        ->orWhere('apellido', 'like', '%'.$bus.'%')
        ->orderBy('id', 'desc')->get();
        return view('student.index', compact('student'));*/
    }

    
   
}
