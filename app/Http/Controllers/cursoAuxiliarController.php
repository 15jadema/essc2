<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class cursoAuxiliarController extends Controller
{
    public function show($id)
    {       
        $curso = DB::table('cursos')
        ->join('users','cursos.docente_id','=','users.username')
        ->where('unidadAprendizaje_id','=',$id)
        ->where('cursos.periodo_id','=',14)
        ->orderby('cursos.id','desc')
        ->get(array('cursos.id', 'cursos.nomCurso','users.username','cursos.codigo','users.nombre','users.nombre2', 'users.apellido', 'users.apellido2'));  
        return view('cursoAuxiliar.index', compact('curso'));
       // return "responde desde indez de cursoAuxiliar";
    }
}
