<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\estado;
use App\Sede;
use App\programa;

class programaController extends Controller
{
  public function create()
  {
    $estado = estado::all();
    $sede = Sede::all();

    return view('programa.create', compact('estado', 'sede'));
  }

  public function store(Request $request)
  {
    $p = new programa();
    $p->nomPrograma = $request->nomPrograma;
    $p->codPrograma = $request->codPrograma;
    $p->abreviatura = $request->abreviatura;
    $p->numResolucion = $request->numResolucion;
    $p->fechaResulucion = $request->fechaResulucion;
    $p->preinscripcion = $request->preinscripcion;
    $p->aplicaGrupos = $request->aplicaGrupos;
    $p->tipoEvaluacion = $request->tipoEvaluacion;
    $p->estado_id = $request->estado_id;
    $p->tipoPrograma = $request->tipoPrograma;
    $p->sede_id = $request->sede_id;
    $p->save();
    return redirect('/programa/');
  }

  public function index()
  {
    $programas = DB::table('programas')->orderBy('nomPrograma', 'asc')->paginate(10);
    // $docente = User::where('levelUser_id',2);
    return view('programa.index', compact('programas'));
  }

  public function mostrarProgramas(Request $request, $id)
  {
    if ($request->ajax()) {
      $programas = programa::programas($id);
      return response()->json($programas);
    }
  }
}
