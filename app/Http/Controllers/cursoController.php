<?php

namespace App\Http\Controllers;
use DB;
use Session;
use App\Curso;
use Illuminate\Http\Request;
use App\Sede;
use App\documento;
use App\Jornada;
use App\Periodo;
use App\programa;
use App\User;
use App\UnidadAprendizaje;
class cursoController extends Controller
{
    public function create()
    {
      $sede = Sede::all();
      $jornada=Jornada::all();
      $programa=Programa::all();      
      $docente=DB::table('users')->where('sede_id', '=',1 )->where('levelUser_id', '=', 2)->where('status_id','=',2)->orderBy('apellido', 'asc')->get();
      $unidad=UnidadAprendizaje::all();
      $periodo=DB::table('periodos')->orderBy('id', 'asc')->get(); 
      return view('curso.create', compact('sede','jornada', 'programa', 'docente','unidad','periodo'));
    }

    public function createCurso($id)
    {
      $sede = Sede::all();
      $jornada=Jornada::all();
      $programa=DB::table('programas')->where('sede_id', '=',$id )->get();   
      $docente=DB::table('users')->where('sede_id', '=',$id )->where('levelUser_id', '=', 2)->where('status_id','=',2)->orderBy('apellido', 'asc')->get();
      $unidad=UnidadAprendizaje::all();
      $periodo=DB::table('periodos')->orderBy('id', 'asc')->get(); 
      return view('curso.create', compact('sede','jornada', 'programa', 'docente','unidad','periodo'));
    }

    public function store(Request $request)
    {
        $c = new Curso();
        $c->codigo = $request->codigo;
        $c->nomCurso = $request->nomCurso;
        $c->docente_id = $request->docente_id;
        $c->jornada_id = $request->jornada_id;
        $c->programa_id = $request->programa_id;
        $c->pensum_id = $request->pensum_id;
        $c->unidadAprendizaje_id = $request->unidadAprendizaje_id;
        $c->cupoMax = $request->cupoMax;
        $c->periodo_id = $request->periodo_id;
        $c->fechaInicio = $request->fechaInicio;
        $c->fechaFinal = $request->fechaFinal;
        $c->estado_id = $request->estado_id;
        $c->sede_id = $request->sede_id;
        $c->archivado_id=0;
        $c->save();
        return redirect('/curso/')->with('success', 'Curso Registrado Satisfactoriamente');
    }

    public function index()
    {
        $curso = DB::table('cursos')
        ->join('users', 'cursos.docente_id', '=', 'users.username')
        ->join('periodos','cursos.periodo_id','=','periodos.id')
        ->join('unidad_aprendizajes','cursos.unidadAprendizaje_id','=','unidad_aprendizajes.id') 
        ->where('cursos.sede_id','=','1')
        ->orderBy('cursos.id','desc')      
        ->paginate(20,array('cursos.id', 'cursos.nomCurso','users.username','cursos.codigo','users.nombre','users.nombre2', 'users.apellido', 'users.apellido2','periodos.nomPeriodo','unidad_aprendizajes.nomUnidadAprendizaje'));
        return view('curso.index', compact('curso'));
    }

    public function indexSede($id)
    {
        $curso = DB::table('cursos')
        ->join('users', 'cursos.docente_id', '=', 'users.username')
        ->join('periodos','cursos.periodo_id','=','periodos.id')
        ->join('unidad_aprendizajes','cursos.unidadAprendizaje_id','=','unidad_aprendizajes.id') 
        ->where('cursos.sede_id','=',$id)
        ->orderBy('periodos.id','desc')    
        ->orderBy('cursos.id','desc')        
        ->paginate(20,array('cursos.id', 'cursos.nomCurso','users.username','cursos.codigo','users.nombre','users.nombre2', 'users.apellido', 'users.apellido2','periodos.nomPeriodo','unidad_aprendizajes.nomUnidadAprendizaje'));
        return view('curso.index', compact('curso'));
    }

    public function edit(Curso $curso)
    {        
        $sede = Sede::all();
        $jornada=Jornada::all();
        $programa=Programa::all();
        $docente=DB::table('users')->where('levelUser_id', '=', 2)->where('status_id','=',1)->orderBy('apellido', 'asc')->get();
        $unidad=UnidadAprendizaje::all();
        $periodo=DB::table('periodos')->orderBy('id', 'asc')->get(); 

        return view('curso.edit', compact('sede','jornada', 'programa', 'docente','unidad','periodo','curso'));
    }

    public function mostrarPeriodo(Request $request, $id)
    {
       if ($request->ajax()) {
            $periodos = Periodo::periodos($id);
            return response()->json($periodos);
        }
       
    }
}
