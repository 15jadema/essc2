<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    public static function notas($id){

       $misNotas = DB::table('notas')
       ->join('users','notas.username','=','users.username')
       ->join('cursos','notas.codigoGrupo','=','cursos.id')
       ->join('unidad_aprendizajes','cursos.unidadAprendizaje_id','=','unidad_aprendizajes.id')
        ->where('notas.username','=',$id)
     /*   ->join('cursos','codigoGrupo','=','cursos.id')
        ->join('users','notas.username','=','users.username')
        ->join('unidad_aprendizaje','cursos.unidadAprendizaje_id','=','unidad_aprendizaje.id')
        ->join('users','curso.docente_id','=','users.username')
        ->join('periodos','curso.periodo_id','=','periodos.id')
        ->where('users.username','=',1004134590)*/
        ->get();
      return ($misNotas);
      
    }
}
