<!DOCTYPE html>
<html>
<head>
	<title>Notas</title>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS --> <!-- bootstrap es opcional es solo para diseño -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

          <!-- Jquery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
            <!-- Jexcel -->
<script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />

<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Material+Icons" />


    
</head>
<body>
	<div class="container mt-5">
	<div class="row mx-auto">
        <div class="form-group col-md-3">
            <label class="form-label">Seleccione el grupo</label>
            <select id="seleccionGrupo" class="form-control"></select>
        </div>
		<div class="form-group col-md-5" style="display: none;">
			<label class="form-label">Seleccione la unidad de aprendizaje</label>
			<select id="seleccionUnidad" class="form-control"></select>
		</div>
	</div>
	<div id="tabExcel"></div>
	<p id="log" class="alert alert-success"></p>
    </div>
    


</body>

<script>

$(document).ready(function() {
     cargarDatos(17);
 $.ajax({
    url: "{{url('api/notas/getGroups/')}}",
    success: function(data){
        console.log(data);
        var html='';
       for (var i = 0; i < data.length; i++) {
        if(data[i].codigoGrupo!=null){
            html+='<option  value="'+data[i].codigoGrupo+'">'+data[i].codigoGrupo+'</option>';
        }else{
            html+='<option value="null">Sin grupo</option>';

        } 
        
       }
       $('#seleccionGrupo').html(html);    
    },
    error:function(){

    }
 });
 function consultaAsignaciones(group){
 $.ajax({
 	url: "{{url('api/notas/getUnits/')}}/"+group,
 	success: function(data){
 		console.log(data);
 		var html='';
       for (var i = 0; i < data.length; i++) {
       	if(data[i].unidadAprendizaje_id!=null){
       		html+='<option value="'+data[i].codigoGrupo+'_$_'+data[i].unidadAprendizaje_id+'">'+data[i].unidadAprendizaje_id+'</option>';
       	}else{
            html+='<option value="null">Sin Unidad</option>';

       	} 
       	
       }
       $('#seleccionUnidad').closest('.form-group').show();
       $('#seleccionUnidad').html(html);	
 	},
 	error:function(){

 	}
 });
 } 

 $('#seleccionGrupo').change(function(event) {
 $('#tabExcel').html('');
 consultaAsignaciones($(this).val());
    
 });

 $('#seleccionUnidad').change(function(event) {
 $('#tabExcel').html('');
 var grupo=$(this).val().split('_$_')[0];
 var unidad=$(this).val().split('_$_')[1];
 cargarDatos(118,70); 	
 });
 });

function cargarDatos(group,Unit){
 var changed = function(instance, cell, x, y, value) {
    var cellName = jexcel.getColumnNameFromId([x,y]);
    $('#log').text('');
    $('#log').text('Nuevo cambio en la celda ' +x+''+y+' valor '+cellName+'');

    var datos=table.getRowData(y);
    console.log(Object.assign({},datos));
    var link='{{url("api/notas/actualizar")}}/'+datos[2]+'/'+datos[1]+'/'+datos[3];
    $.ajax({
    	url: link,
    	type: 'post',
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
    		data: {
    			valores:datos,
    		},
    		success: function(){
               console.log('Datos actualizados');
    		},
    		error: function(){
                console.log('Datos no se pudieron actualizar');
    		}
    });
    
}

var beforeChange = function(instance, cell, x, y, value) {
    var cellName = jexcel.getColumnNameFromId([x,y]);
    $('#log').text('');
    $('#log').text('La celda ' + cellName + 'sera cambiada');
}

var insertedRow = function(instance, cell) {
    $('#log').text('');
    $('#log').text('Fila Agregada');
    // var array=JSON.parse(window.localStorage.getItem("valoresPredeterminados"));
    // console.log(array);
    // var link='{{url("api/notas/crear")}}/';
    // $.ajax({
    // 	    url: link,
    //         type: 'post',
    //         headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         data: {
    //             valores:[array[1],array[3]],
    //         },
    // 		success: function(data){
    // 			console.log(data.id);
    // 		   table.setRowData(cell,[data.id,array[1],"",array[3],]);
    //            console.log('Dato creado');
    // 		},
    // 		error: function(){
    //             console.log('Dato no se pudo crear');
    // 		}
    // });
}

var insertedColumn = function(instance, cell, x, y) {
    $('#log').text('');
    $('#log').text('Columna Agregada');

}
var deletedRow = function(instance,cell) {
    $('#log').text('');
    $('#log').text('Fila Eliminada');
    console.log(table.getRowData(cell));
     var datos=table.getRowData(cell);
     var link='{{url("api/notas/eliminar")}}/'+datos[2]+'/'+datos[1]+'/'+datos[3];
    $.ajax({
    	    url: link,
    	    type: 'post',
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
    		success: function(data){
               console.log('Dato eliminado');
               return true;
    		},
    		error: function(){
                console.log('Dato no se pudo eliminar');
                return false;
    		}
    });

}

var deletedColumn = function(instance) {
    $('#log').text('');
    $('#log').text('Columna eliminada');
}

var sort = function(instance, cellNum, order) {
    var order = (order) ? 'desc' : 'asc';
    $('#log').text('');
    $('#log').text('La columna  ' + cellNum + ' se ordeno ' + order + '');
}

var resizeColumn = function(instance, cell, width) {
    $('#log').text('');
    $('#log').text('La columna  ' + cell + ' cambio el ancho ' + width + ' px');
}

var resizeRow = function(instance, cell, height) {
    $('#log').text('');
    $('#log').text('La fila  ' + cell + ' cambio el alto ' + height + ' px');
}

var selectionActive = function(instance, x1, y1, x2, y2, origin) {
    var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
    var cellName2 = jexcel.getColumnNameFromId([x2, y2]);
    $('#log').text('');
    $('#log').text('La selección de  ' + cellName1 + ' a ' + cellName2 + '');
}

var loaded = function(instance) {
    $('#log').text('');
    $('#log').text('Nuevos datos cargados');
}

var moveRow = function(instance, from, to) {
    $('#log').text('');
    $('#log').text('La fila ' + from + ' fue movida a la posición ' + to + ' ');
}

var moveColumn = function(instance, from, to) {
    $('#log').text('');
    $('#log').text('La col ' + from + ' fue movida a la posición ' + to + ' ');
}

var blur = function(instance) {
    $('#log').text('');
    $('#log').text('La tabla ' + $(instance).prop('id') + ' es difuminada');
}

var focus = function(instance) {
    $('#log').text('');
    $('#log').text('La tabla ' + $(instance).prop('id') + ' es seleccionada');
}

var table='';
$.ajax({
    url: "{{url('api/notas/getAll')}}/"+group+"/"+Unit,
    success: function(data){
      var datos=data;
     console.log(datos);

table=jexcel(document.getElementById('tabExcel'), {
    data:datos[0],
    //search:true,
    lazyLoading:true,
    loadingSpin:true,
    defaultColWidth: 100,
    tableOverflow: true,
    tableWidth: "1200px",
    columns: datos[1],
    nestedHeaders:datos[2],
    columnDrag:true,
    allowInsertRow:false,
    allowRenameColumn:false,
    allowDeleteColumn:false,
    onbeforeinsertcolumn:false,
    ondeletecolumn:false,
    onchange:changed,
    onbeforechange: beforeChange,
    oninsertrow: insertedRow,
    oninsertcolumn: insertedColumn,
    onbeforedeleterow: deletedRow,
    ondeletecolumn: deletedColumn,
    onselection: selectionActive,
    onsort: sort,
    onresizerow: resizeRow,
    onresizecolumn: resizeColumn,
    onmoverow: moveRow,
    onmovecolumn: moveColumn,
    onload: loaded,
    onblur: blur,
    onfocus: focus,
   /* toolbar:[
        {
            type: 'i',
            content: 'undo',
            onclick: function() {
                table.undo();
            }
        },
        {
            type: 'i',
            content: 'redo',
            onclick: function() {
                table.redo();
            }
        },
        {
            type: 'i',
            content: 'save',
            onclick: function () {
                table.download();
            }
        },
        {
            type: 'select',
            k: 'font-family',
            v: ['Arial','Verdana']
        },
        {
            type: 'select',
            k: 'font-size',
            v: ['9px','10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px']
        },
        {
            type: 'i',
            content: 'format_align_left',
            k: 'text-align',
            v: 'left'
        },
        {
            type:'i',
            content:'format_align_center',
            k:'text-align',
            v:'center'
        },
        {
            type: 'i',
            content: 'format_align_right', 
            k: 'text-align',
            v: 'right'
        },
        {
            type: 'i',
            content: 'format_bold',
            k: 'font-weight',
            v: 'bold'
        },
        {
            type: 'color',
            content: 'format_color_text',
            k: 'color'
        },
        {
            type: 'color',
            content: 'format_color_fill',
            k: 'background-color'
        },
    ],*/
        text:{

  areYouSureToDeleteTheSelectedRows:'Estas seguro de eliminar estas filas?',

},


});
//redundancia...
/*table.setData(datos[0]);
var c=0;
for (var i = 0; i < datos[0].length; i++) {
    table.setRowData(c,datos[0][i]);
    

    if (c==0 && typeof(Storage) !== "undefined") {
         var valores=datos[0][i];
         window.localStorage.setItem("valoresPredeterminados", JSON.stringify(valores));
       
    }
    c++;
}*/
    },
    error:function(){
        console.log('error al cargar los datos')
    }

});

}


</script>
    

</html>