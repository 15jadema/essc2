@extends('layouts.plantilla') @section('title','Administrador detalle') @section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-8">

        </div>

        <div class="col-2 text-right">
            <h5>{{$administrador->nombre}} {{$administrador->nombre2}}</h5>
            <h6>{{$administrador->apellido}} {{$administrador->apellido2}}</h6>
            <h6>Documento: {{$administrador->username}}</h6>
        </div>

        <div class="col-2">
            <div class="card" style="width:155px;">
                <img src="/images/{{$administrador->foto}}">
            </div>

        </div>

    </div>

</div>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Información Personal</a>
    </li>
    <li class="nav-item">
        <a href="{{ Route('periodo.create')}}" class="nav-link" id="profile-tab" data-toggle="tab" role="tab" aria-controls="profile" aria-selected="false">Información Académica</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

        <div class="row">
            <div class="col-3 mt-5">
                <p class="text-xl-left">Nombres: {{$administrador->nombre}} {{$administrador->nombre2}}</p>
                <p class="mt-1">Identificacion: {{$administrador->username}}</p>
                <p>Telefono: {{$administrador->telefono}}</p>
                <p>Correo: {{$administrador->email}}</p>
            </div>
            <div class="col-3">

            </div>
            <div class="col-3 mt-5">
                <p class="text-xl-left">Apellidos: {{$administrador->apellido}} {{$administrador->apellido2}}</p>
                <p class="mt-1">Sexo: {{$administrador->genero_id}}</p>
                <p>Celular: {{$administrador->celular}}</p>
                <p>Direccion: {{$administrador->direccion}}</p>
            </div>
            <div class="col-3">
                <td><a href="/administrador/ {{$administrador->id}}/edit" class="btn btn-warning">Editar</a></td>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="row">
            <div class="col-3">
                <a class="" href="{{ Route('programa.create')}}">Programas</a>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <input type="text" value="{{$administrador->username}}" id="username" name="username">
        <form action="/unidadAprendizaje/">
            Codigo programa:

            <input type="text" name="username" value="{{$administrador->username}}">
            <button type="submit" class="btn btn-primary">Editar</button>
        </form>
    </div>
</div>
</div>
@endsection