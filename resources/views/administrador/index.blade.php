@extends('layouts.plantilla')
@section('title','Menu Listado')

@section('content')
<div class="container">
        <p>Listado de Docentes</p>
<div class="mb-sm-2"><a class="btn btn-primary" href="{{ Route('administrador.create')}}" >Registrar Administrativo</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Documentos</th>
      <th scope="col">Apellidos y Nombres</th>      
      <th scope="col">Contacto</th>
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($administrador as $admin)
    <tr>
      <td scope="row">{{$admin->username }}</td>
      <td scope="row">{{$admin->apellido }} {{$admin->apellido2 }} {{$admin->nombre }} {{$admin->nombre2 }}</td>
      <td scope="row">{{$admin->celular }} --- {{$admin->celular }}</td>
      <td><a href="/administrador/{{$admin->id}}" class="btn btn-primary">Detalle</a></td>
       </tr>
  @endforeach
  </tbody>
</table>

</div>
@endsection

