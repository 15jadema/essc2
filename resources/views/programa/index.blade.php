@extends('layouts.plantilla')
@section('title','Menu Listado')

@section('content')
<div class="container">
        <h4>Listado Programas Técnicos</h4>
<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{-- Route('student.create')--}}" >Registrar Programa</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th>N°</th>
       <th scope="col">Nombre Programa Técnico</th>  
      <th scope="col">Codigo Programa</th>         
      <th scope="col">Abreviatura</th>
      <th scope="col">Ver Detalles</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($programas as $pro)
    <tr>
      <td>{{$loop->index+1}}</td>      
      <td scope="row"><a href="/asignacionUnidad/{{$pro->id}}">{{$pro->nomPrograma }}</a></td>
      <td scope="row">{{$pro->codPrograma }}</td>
      <td scope="row">{{$pro->abreviatura }}</td>
      <td><a href="#" class="btn btn-primary" locked>Detalle</a></td>
      <!--<td><a href="href="/programa/{{--$pro->id--}}" class="btn btn-warning">Editar</a></td>-->
    </tr>
  @endforeach
  </tbody>
</table>

</div>
@endsection

