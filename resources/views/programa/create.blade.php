@extends('layouts.plantilla') @section('title','Menu de Estructura') @section('content')
<!--
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/programa.js"></script>-->
<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Crear Programa Técnico</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/programa" class="form-group" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Código</label>
                            <input type="text" class="form-control" name="codPrograma" id="codPrograma" placeholder="Codigo">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nombre Programa</label>
                            <input type="text" class="form-control" id="nomPrograma" name="nomPrograma" aria-describedby="emailHelp" placeholder="Nombre">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Abreviación</label>
                            <input type="text" class="form-control" name="abreviatura" id="abreviatura" placeholder="Abreviación">

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Estado</label>
                            <select class="form-control" name="estado_id" id="estado_id">
                            @foreach ($estado as $es)
                <option value="{{$es->id}}">{{$es->nomEstado}}</option>
            @endforeach  
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha  Resolución</label>
                            <input type="date" class="form-control" name="fechaResulucion" id="fechaResulucion">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Número Resolución</label>
                            <input type="text" class="form-control" name="numResolucion" id="numResolucion">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Preinscripción</label>
                            <select class="form-control" name="preinscripcion" id="preinscripcion">
                     
                <option value="1">No</option>
                <option value="2">Si</option>
           
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Tipo de Evaluación</label>
                            <select class="form-control" name="tipoEvaluacion" id="tipoEvaluacion">
                            <option value="1">Cuantitativa</option>
           
                                  </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Sede</label>
                            <select class="form-control" name="sede_id" id="sede_id">
                                @foreach ($sede as $se)
                    <option value="{{$se->id}}">{{$se->nomSede}}</option>
                @endforeach  
                               </select>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
            </form>
        </div>
    </div>


    @endsection