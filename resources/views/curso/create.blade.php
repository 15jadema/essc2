@extends('layouts.plantilla') @section('title','Menu de Estructura') @section('content')
<!--
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>-->
<script src="/js/programa.js"></script>
<script src="/js/periodo.js"></script>

<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Crear Curso</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/curso" class="form-group" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Código(*)</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Codigo">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nombre(*)</label>
                            <input type="text" class="form-control" id="nomCurso" name="nomCurso" aria-describedby="emailHelp" placeholder="Nombre">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Docente(*)</label>
                            <select class="form-control" name="docente_id" id="docente_id">
           @foreach ($docente as $doc)
                <option value="{{$doc->username}}"> {{$doc->apellido}} {{$doc->apellido2}} {{$doc->nombre}} {{$doc->nombre2}} </option>
                @endforeach
                           </select>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Sede - Jornada(*)</label>
                            <select class="form-control" name="jornada_id" id="jornada_id">                           
                             @foreach ($jornada as $jor)
                <option value="{{$jor->id}}">{{$jor->nomJornada}}</option>
                @endforeach
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Programa(*)</label>
                            <select class="form-control" name="programa_id" id="programa_id">
                             @foreach ($programa as $pro)
                <option value="{{$pro->id}}">{{$pro->nomPrograma}}</option>
                @endforeach
           
         </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Pensum(*)</label>
                            <select class="form-control" name="pensum_id" id="pensum_id">
                            <option value="1">Individual</option>
           
         </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Unidad de Aprendizaje(*)</label>
                            <select class="form-control" name="unidadAprendizaje_id" id="unidadAprendizaje_id">
                             @foreach ($unidad as $un)
                <option value="{{$un->id}}">{{$un->nomUnidadAprendizaje}}</option>
                @endforeach
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Cupo Máximo</label>
                            <input type="number" id="cupoMax" name="cupoMax" class="form-control" value="10">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Periodo</label>
                            <select class="form-control" name="periodo_id" id="periodo_id">
                                @foreach ($periodo as $per)
                   <option value="{{$per->id}}">{{$per->nomPeriodo}}</option>
                   @endforeach
                              </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha Incio</label>
                            <input type="date" id="fechaInicio" name="fechaInicio" class="form-control">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha Final</label>
                            <input type="date" id="fechaFinal" name="fechaFinal" class="form-control">
                        </div>
                    </div>
            </div>

            <input type="hidden" id="estado_id" name="estado_id" value="1">
            <input type="hidden" id="sede_id" name="sede_id" value="{{Session::get('sede_id')}}">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-primary">Registrar Curso</button>
            </div>
            </form>
        </div>
    </div>


    @endsection