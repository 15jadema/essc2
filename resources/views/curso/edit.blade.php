@extends('layouts.plantilla') @section('title','Menu de Estructura') @section('content')
<script src="/js/docentes.js"></script>
<script src="/js/periodoOnly.js"></script>
<script src="/js/unidadAprendizaje.js"></script>
<script src="/js/programaOnly.js"></script>
<script src="/js/jornada.js"></script>

<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Editar Curso</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/curso" class="form-group" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="sede_id" name="sede_id" value="{{Session::get('sede_id')}}">
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Código Curso</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Codigo" value="{{$curso->codigo}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input value="{{$curso->nomCurso}}" type="text" class="form-control" id="nomCurso" name="nomCurso" aria-describedby="emailHelp" placeholder="Nombre">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Docente</label>
                            <input type="hidden" id="docente_id_text" value="{{$curso->docente_id}}">
                            <select class="form-control" name="docente_id" id="docente_id">
           @foreach ($docente as $doc)
                <option value="{{$doc->username}}"> {{$doc->apellido}} {{$doc->apellido2}} {{$doc->nombre}} {{$doc->nombre2}} </option>
                @endforeach
                           </select>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                         <input type="hidden" id="jornada_id_text" value="{{$curso->jornada_id}}">
                            <label for="exampleInputEmail1">Jornada</label>
                            <select class="form-control" name="jornada_id" id="jornada_id">                           
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Programa</label>
                            <input type="hidden" id="programa_id_text" value="{{$curso->programa_id}}">
                            <select class="form-control" name="programa_id" id="programa_id">
                             @foreach ($programa as $pro)
                <option value="{{$pro->id}}">{{$pro->nomPrograma}}</option>
                @endforeach
           
         </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Pensum</label>
                            <select class="form-control" name="pensum_id" id="pensum_id">
                            <option value="1">Individual</option>
           
         </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Unidad de Aprendizaje</label>
                            <input type="hidden" id="unidadAprendizaje_id_text" value="{{$curso->unidadAprendizaje_id}}">
                            <select class="form-control" name="unidadAprendizaje_id" id="unidadAprendizaje_id">
                             @foreach ($unidad as $un)
                <option value="{{$un->id}}">{{$un->nomUnidadAprendizaje}}</option>
                @endforeach
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Cupo Maximo</label>
                            <input type="number" id="cupoMax" name="cupoMax" class="form-control" value="{{$curso->cupoMax}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Periodo</label>
                            <input type="hidden" id="periodo_id_text" value="{{$curso->periodo_id}}">
                            <select class="form-control" name="periodo_id" id="periodo_id">
                                @foreach ($periodo as $per)
                   <option value="{{$per->id}}">{{$per->nomPeriodo}}</option>
                   @endforeach
                              </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha Incio</label>
                            <input type="date" id="fechaInicio" name="fechaInicio" class="form-control" value="{{$curso->fechaInicio}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha Final</label>
                            <input type="date" id="fechaFinal" name="fechaFinal" class="form-control" value="{{$curso->fechaFinal}}">
                        </div>
                    </div>
            </div>

            <input type="hidden" id="estado_id" name="estado_id" value="1">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>


            </form>
        </div>
    </div>
    @endsection