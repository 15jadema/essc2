@extends('layouts.plantilla') @section('title','Menu Listado') @section('content')
<div class="container">
{{--dd($curso)--}}
    <p>Listado Curso</p>
    @if(Session::has('success'))
            <div class="alert alert-info">
                {{Session::get('success')}}
            </div>
            @endif

    <div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('curso.create')}}">Curso Nuevo</a></div>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Codigo</th>
                <th scope="col">Periodo</th>
                <th scope="col">Curso</th>
                <th scope="col">Docente</th>
                <th scope="col">Unidad Aprendizaje</th>
                <th scope="col">Listado</th>
                <th scope="col">Detalles</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($curso as $cur)
            <tr>

                <td scope="row">{{$cur->codigo}}</td>
                <td scope="row">{{$cur->nomPeriodo }}</td>
                <td scope="row">{{$cur->nomCurso }}</td>
                <td scope="row">{{$cur->apellido }} {{$cur->apellido2 }} {{$cur->nombre }} {{$cur->nombre2 }}</td>
                <td scope="row">{{$cur->nomUnidadAprendizaje }}</td>
                <td><a href="/matricula/{{$cur->id}}" class="btn btn-warning">Matriculados</a></td>
                <td><a href="#" data-curso="{{$cur->id}}"class="btn btn-danger cursos">Cursos</a></td>
                <td><a href="/curso/{{$cur->id}}/edit" class="btn btn-primary">Detalle</a></td>

            </tr>
            @endforeach
        </tbody>
    </table>
{!! $curso->render() !!}
    <!--  modal de notas -->
 <div class="modal fade" id="notas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Notas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$('a[class*="cursos"]').click(function(event) {
    let curso=$(this).data("curso");
    console.log(curso);
    let link="{{url('/notas-ver/')}}/"+curso;
    $.ajax({
               url: link,
               success: function(result){
                   console.log(result);
                  $("#notas.modal-body").html(result.html);
                  $("#notas").modal("show");
                }
     });


});
</script>
@endsection
