@extends('layouts.plantilla') @section('title','Menu Listado') @section('content')
<script src="/js/pruebaMatricula.js"></script>
<div class="container">{{--dd($curso)--}}
    <br>
    <h4>Listado Curso disponibles a Matricular</h4>
    <!--<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('curso.create')}}">Curso Nuevo</a></div>-->
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Codigo</th>
                <th scope="col">Curso</th>
                <th scope="col">Nombre Curso</th>
                <th scope="col">Docente</th>
                <th scope="col">Matricular Curso</th>
                <th scope="col">Ver</th>
            </tr>
        </thead>
        <tbody>
        <label>Estudiante: {{ Session::get('usernameEst')}}</label>
        <input type="hidden" value="{{ Session::get('usernameEst')}}" id="username" name="username"> @foreach ($curso as $cur)
            <!--<form action="/matricula" class="form-group" method="POST">-->
            @csrf
            <tr>
                <td scope="row">{{$cur->id }}</td>
                <td scope="row">{{$cur->codigo }}</td>
                <td scope="row">{{$cur->nomCurso }}</td>
                <td scope="row">{{$cur->nombre }} {{$cur->nombre2 }} {{$cur->apellido }} {{$cur->apellido2 }} </td>
                <td> <a id="{{$cur->id}}" href="#" class="btn btn-primary vinculo">Matricular en Curso</a></td>
                <td><a href="/matricula/{{$cur->id}}" class="btn btn-warning">Matriculados</a></td>
            </tr>
            <!--</form>-->
            @endforeach
        </tbody>
    </table>
</div>
@endsection