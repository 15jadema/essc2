$(document).ready(function() {

    $('#xxx').click(function(e) {
        e.preventDefault();
        alert("Esta respondiendo desde el script");

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btnSave').click(function(e) {
        alert("Esta respondiendo desde el script");

        e.preventDefault();

        $(this).html('Sending..');



        $.ajax({
            data: $('#formularioRegistro').serialize(),
            url: "{{ route('student.store') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                $('#formularioRegistro').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
            },

            error: function(data) {
                // console.log('Error:', data);
                $('#saveBtn').html('Save Changes');
            }

        });

    });
})