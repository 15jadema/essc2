$(document).ready(function() {

    if ($("#sedeX_id").val() != null) {
        id = $("#sedeX_id").val();
        idX = $("#programaX_id").val();

        ///  $.get('/programasAll', function(datag) {
        $.get('/programas/' + id, function(datag) {
            $("#programa_id").empty();
            for (let i = 0; i < datag.length; i++) {
                if (datag[i].id == idX) {
                    $("#programa_id").append("<option value='" + datag[i].id + "' selected>" + datag[i].nomPrograma + "</option>");
                } else {
                    $("#programa_id").append("<option value='" + datag[i].id + "'>" + datag[i].nomPrograma + "</option>");
                }
            }
        });

    } else {
        //nuevo

        id = $("#sede_id").val();

        $.get('/programas/' + id, function(data) {
            console.log("Resultado programa: " + data.length);
            // alert(data.length);
            $("#programa_id").empty();
            for (let i = 0; i < data.length; i++) {
                $("#programa_id ").append("<option value='" + data[i].id + "'>" + data[i].nomPrograma + "</option>");
            }
        });

        $("#sede_id").change(function(event) {
            id = $("#sede_id").val();
            //alert(id);
            $.get('/programas/' + id, function(data) {

                //   console.log(data.length);
                $("#programa_id").empty();
                for (let i = 0; i < data.length; i++) {
                    $("#programa_id").append("<option value='" + data[i].id + "'>" + data[i].nomPrograma + "</option>");
                }
            });
        });
    }

});