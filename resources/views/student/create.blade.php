@extends('layouts.plantilla') @section('title','Menu de registro') @section('content')


<script src="/js/sede.js"></script>
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/programa.js"></script>

<div class="container">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalCenterTitle">Formulario Registro Estudiante</h5>



                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                      </button>

            </div>

            <div class="modal-body">

           @include('student.errors') 

                <form action="/student" class="form-group" method="POST" enctype="multipart/form-data">

                    @csrf

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Primer Apellido </label><label class="text-danger"><strong> (*)</strong></label>

                            <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Primer Apellido Estudiante">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Segundo Apellido</label>

                            <input type="text" class="form-control" name="apellido2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Segundo Apellido Estudiante">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Primer nombre </label><label class="text-danger"><strong> (*)</strong></label>

                            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Primer Nombre Estudiante">



                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Segundo Apellido</label>

                            <input type="text" class="form-control" name="nombre2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Segudno Apellido Estudiante">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Tipo Documento</label>

                            <select class="form-control" name="documento_id" id="documento_id">

                                @foreach ($documento as $doc)

                                    <option value="{{$doc->id}}">{{$doc->documento}}</option>

                                @endforeach  

                            </select>



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Número Documento </label> <label class="text-danger"><strong> (*)</strong></label>

                            <input type="number" class="form-control" name="username" id="username" placeholder="Número Documento">

                        </div>

                    </div>



                    <div class="row">

                        <div class="form-group col-4">

                            <label for="">Sexo</label>

                            <select class="form-control" name="genero_id" id="genero_id">

                                @foreach ($genero as $sex)

                            <option value="{{$sex->id}}">{{$sex->sexo}}</option>

                        @endforeach 

                     </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Correo Electrónico </label> <label class="text-danger"><strong> (*)</strong></label>

                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Correo Electrónico">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Teléfono</label>

                            <input type="number" class="form-control" id="telefono" name="telefono" aria-describedby="emailHelp" placeholder="Teléfono">



                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Celular </label><label class="text-danger"><strong> (*)</strong></label>

                            <input type="number" class="form-control" id="celular" name="celular" aria-describedby="emailHelp" placeholder="Celular">
                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Fecha de Nacimiento </label><label class="text-danger"><strong> (*)</strong></label>

                            <input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento">
                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Departamento nacimiento</label>

                            <select class="form-control" name="depaNacimiento_id" id="depaNacimiento_id">

                                @foreach ($departamento as $d)

                                <option value="{{$d->id}}">{{$d->departamento}}</option>

                            @endforeach    

                         </select>



                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Municipio nacimiento</label>

                            <select class="form-control" name="munNacimiento_id" id="munNacimiento_id" placeholder="Selecciona Municipio">

                            </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Dirección Residencia</label>

                            <input type="text" class="form-control" name="direccion" id="direccion" aria-describedby="emailHelp" placeholder="Direccion Residencia">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Barrio</label>

                            <input type="text" class="form-control" name="barrio" id="barrio" aria-describedby="emailHelp" placeholder="Barrio Recidencia">

                     

                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Periodo Académico</label>

                           <select class="form-control" name="periodo_id" id="periodo_id">

                                    @foreach ($periodo as $per)

                                        <option value="{{$per->id}}">{{$per->nomPeriodo}}</option>

                                    @endforeach  

                                 </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Sede</label>

                            <select class="form-control" name="sede_id" id="sede_id">                              

                            </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Programa Académico</label>

                            <select name="programa_id" id="programa_id" class="form-control">                            
                            </select>

                        </div>



                        <input value="3" type="hidden" class="form-control" name="levelUser_id" id="levelUser_id">
                    

                    </div>

            </div>

            <div class="modal-footer">

               <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->



                <button type="submit" class="btn btn-primary">Registrar</button>

            </div>

            </form>

        </div>

    </div>





    @endsection