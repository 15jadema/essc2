@extends('layouts.plantilla') @section('title','Editar Estudiante') @section('content')

<script src="/js/sede.js"></script>

<script src="/js/periodo.js"></script>

<script src="/js/municipios.js"></script>

<script src="/js/documento.js"></script>

<script src="/js/genero.js"></script>

<script src="/js/programa.js"></script>

<script src="/js/estado.js"></script>



<!-- Button trigger modal -->

<!-- Modal -->

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Estudiante</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">&times;</span>

        </button>

            </div>



            <form id="delete" action="/student/{{$student->id}}" class="form-group" method="POST" enctype="multipart/form-data">

                <div class="modal-body">

                    {{ csrf_field() }} @method('DELETE') Realmente desea <strong class="text text-danger">eliminar</strong> al estudiante: <b> {{$student->apellidos}}  {{$student->nombres}} </b> ?

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>

                </div>

            </form>

        </div>

    </div>

</div>



<div class="container">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalCenterTitle">Información Estudiante</h5>



                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                      </button>

            </div>

            <div class="modal-body">

                <form action="/student/{{$student->id}}" class="form-group" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }} @method('PUT') @csrf

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Estado</label>

                            <input type="hidden" id="estado_id_text" value="{{$student->status_id}}" />

                            <select class="form-control" name="status_id" id="status_id">                                 

                            </select>



                        </div>

                    </div>
                    <hr>
                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Primer Apellido</label>

                            <input type="text" class="form-control" name="apellido" id="apellidos" placeholder="Primer apellido" value="{{$student->apellido}}">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Segundo Apellido</label>

                            <input type="text" class="form-control" name="apellido2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Segundo Apellido" value="{{$student->apellido2}}">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Primer nombre</label>

                            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Primer nombre" value="{{$student->nombre}}">



                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Segundo nombre</label>

                            <input type="text" class="form-control" name="nombre2" id="nombre2" aria-describedby="emailHelp" placeholder="Segundo nombre" value="{{$student->nombre2}}">

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Tipo Documento</label>

                            <input type="hidden" id="documento_id_text" value="{{$student->documento_id}}" />

                            <select class="form-control" name="documento_id" id="documento_id">                                 

                            </select>



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Documento</label>

                            <input disabled type="number" class="form-control" name="username" id="username" placeholder="Documento" value="{{$student->username}}">

                        </div>

                    </div>



                    <div class="row">

                        <div class="form-group col-4">

                            <label for="">Sexo</label>

                            <input type="hidden" id="genero_id_text" value="{{$student->genero_id}}" />

                            <select class="form-control" name="genero_id" id="genero_id">

                            </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Correo Electrónico</label>

                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Correo electrónico" value="{{$student->email}}">



                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Teléfono</label>

                            <input type="text" class="form-control" name="telefono" id="telefono" aria-describedby="emailHelp" placeholder="Teléfono" value="{{$student->telefono}}">

                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Celular</label>

                            <input type="text" class="form-control" name="celular" id="celular" aria-describedby="emailHelp" placeholder="Celular" value="{{$student->celular}}">

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Fecha de Nacimiento</label>

                            <input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento" value="{{$student->fechaNacimiento}}">

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Municipio Nacimiento</label>

                            <input type="hidden" class="form-control" name="munNacimientoX_id" id="munNacimientoX_id" value="{{$student->munNacimiento_id}}">

                            <select class="form-control" name="munNacimiento_id" id="munNacimiento_id">                            

                            </select>

                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Dirección</label>

                            <input type="text" class="form-control" name="direccion" id="direccion" value="{{$student->direccion}}">

                        </div>



                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Barrio</label>

                            <input type="text" class="form-control" name="barrio" id="barrio" aria-describedby="emailHelp" placeholder="Barrio" value="{{$student->barrio}}">

                        </div>



                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Periodo Académico</label>

                            <input disabled type="text" class="form-control" name="periodoX_id" id="periodoX_id" value="{{$student->periodo_id}}">





                        </div>



                    </div>

                    <div class="row">



                        <div class="form-group col-4">

                            <label for="foto">Fotografía</label>

                            <input type="file" class="form-control" name="foto">

                        </div>



                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Sede</label>

                            <input type="hidden" class="form-control" name="sedeX_id" id="sedeX_id" value="{{$student->sede_id}}">

                            <select disabled class="form-control" name="sede_id" id="sede_id">

                            </select>

                        </div>

                        <div class="form-group col-4">

                            <label for="exampleInputEmail1">Programa Académico</label>

                            <input type="hidden" class="form-control" name="programaX_id" id="programaX_id" value="{{$student->programa_id}}">

                            <select class="form-control" name="programa_id" id="programa_id">

                            </select>

                        </div>



                    </div>

            </div>

            <div class="modal-footer">

                <button type="submit" class="btn btn-primary" data-dismiss="modal">Actualizar</button>

                </form>

                <button disabled type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>

            </div>

        </div>

    </div>



    @endsection