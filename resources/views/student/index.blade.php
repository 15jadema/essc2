@extends('layouts.plantilla') @section('title','Menu Listado') @section('content')
<script src="/js/consultaNotas.js"></script>

@if(Session::has('success'))
<div class="alert alert-info">
    {{Session::get('success')}}
</div>
@endif

<div class="container">
    <h4 class="text-primary">Estudiantes</h4>
    <div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('student.create')}}">Registrar Estudiante</a></div>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Documento</th>
                <th scope="col">Apellidos y Nombres</th>
                <th scope="col">Contacto</th>
                <th scope="col">Vinculación</th>
                <th scope="col">Estado</th>
                <th scope="col">Ver Detalle</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($student as $est)
            <tr>
                <!--<td><img class="foto" src="images/"></td>-->
                <td scope="row">{{$est->username }} <input type="hidden" id="username" value="{{$est->username }}"></td>
                <td scope="row">{{$est->apellido }} {{$est->apellido2 }} {{$est->nombre }} {{$est->nombre2 }}</td>
                <td scope="row">{{$est->telefono }} --- {{$est->celular }}</td>
                <td scope="row">
                    @if ($est->levelUser_id==1) Admninstrativo @else @if ($est->levelUser_id==2)
                    <strong class="text text-warning"> Docente</strong> @else Estudiante---<strong>{{$est->abreviatura}}</strong> @endif @endif
                </td>
                <td>
                    @if($est->status_id==2)
                    <strong class="text-success">Activo</strong>
                     @else
                    <strong class="text-danger">Inactivo</strong> 
                    @endif
                </td>
                <td>
                    @if ($est->levelUser_id==1)
                    <a href="/administrativo/{{$est->id}}" class="btn btn-primary">Detalle</a> @else @if ($est->levelUser_id==2)
                    <a href="/docente/{{$est->id}}" class="btn btn-primary">Detalle</a> @else
                    <a href="/student/{{$est->id}}" class="btn btn-primary">Detalle.</a> @endif @endif
                    <!--<a href="#" class="btn btn-warning" id="mostrarNota">Notas</a>-->
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!--<div id="contenedor">..::..</div>-->
</div>
@endsection