 @extends('layouts.plantilla') @section('title','Estudiante detalle') @section('content') @php {{ $someArray = json_decode($student, true);}} @endphp {{--dd($asignatura)--}} {{--dd($student)--}} {{--dd($asignatura)--}}
<script src="/js/consultaNotas.js"></script>
<div class="container mt-5">
    <div class="row">
        <div class="col-8">
            <div class="col-lg-8">
                <h4> {{ $someArray[0]["nomPrograma"]}}</h4>
            </div>
        </div>

        <div class="col-2 text-right">
            <h5>{{ $someArray[0]["nombre"]}} {{ $someArray[0]["nombre2"]}}</h5>
            <h6>{{ $someArray[0]["apellido"]}} {{ $someArray[0]["apellido2"]}}</h6>
            <h6>Documento: {{ $someArray[0]["username"]}}</h6>
        </div>

        <div class="col-2">
            <div class="card" style="width:155px;">
                <img src="/images/{{ $someArray[0]['foto']}}">
            </div>

        </div>

    </div>

</div>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Información Personal</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Informacion Academica</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#record" role="tab" aria-controls="contact" aria-selected="false">Record Academico</a>

    </li>
</ul>


<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

        <div class="row">
            <div class="col-3 mt-5">
                <p class="text-xl-left">Nombres: {{ $someArray[0]["nombre"]}} {{ $someArray[0]["nombre2"]}}</p>
                <p class="mt-1">Identificacion: {{ $someArray[0]["username"]}}</p>
                <p>Telefono:{{ $someArray[0]["telefono"]}} </p>
                <p>Correo: {{ $someArray[0]["email"]}}</p>
                <p>Estado: @if ($someArray[0]["status_id"]==2)
                    <label class="text-success">ACTIVO</label>
                    @else
                     <label class="text-danger">INACTIVO</label>
                @endif</p>               
                 <p>Municipio Residencia: {{ $someArray[0]["munDireccion_id"]}}</p>
            </div>
            <div class="col-3">
            </div>
            <div class="col-3 mt-5">
                <p class="text-xl-left">Apellidos: {{ $someArray[0]["apellido"]}} {{ $someArray[0]["apellido2"]}}</p>
                <p class="mt-1">Sexo: {{ $someArray[0]["sexo"]}}</p>
                <p>Celular: {{ $someArray[0]["celular"]}}</p>
                <p>Direccion: {{ $someArray[0]["direccion"]}}</p>
                <p>Barrio: {{ $someArray[0]["barrio"]}}</p>
                <p>Departamento Residencia: {{ $someArray[0]["depaDireccion_id"]}}</p>
            </div>
            <div class="col-3">
                <td><a href="/student/{{$id}}/edit" class="btn btn-warning">Editar Información</a></td>
            </div>
        </div>
<hr>
        <div class="row">
            <div class="col-3 mt-5">
                <p class="text-xl-left">Fecha Nacimiento: {{ $someArray[0]["fechaNacimiento"]}}</p>
               <p>Departamento Nacimiento: {{ $someArray[0]["depaNacimiento_id"]}}</p>
                <p>Estrato:{{ $someArray[0]["estrato_id"]}} </p>
                <p>Grupo Sanguineo/RH: {{ $someArray[0]["rh_id"]}}</p>  
                <p>Estado Civil: {{ $someArray[0]["estadoCivil_id"]}}</p>               
            </div>
            <div class="col-3">
            </div>
            <div class="col-3 mt-5">
                <p class="text-xl-left">Municipio Nacimiento: {{ $someArray[0]["nombreFamiliar"]}}</p>
                <p class="mt-1">Sisben: {{ $someArray[0]["sisben_id"]}}</p>
                <p>Eps {{ $someArray[0]["eps"]}}</p>              
                <p>Zona: {{ $someArray[0]["zona_id"]}}</p>
            </div>           
        </div>
<hr>
         <div class="row">
            <div class="col-3 mt-5">
                <p class="text-xl-left">Familiar/Acudiente: {{ $someArray[0]["nombreFamiliar"]}}</p>
                <p>Relación: {{ $someArray[0]["relacionFamiliar_id"]}}</p>                         
            </div>
            <div class="col-3">
            </div>
            <div class="col-3 mt-5">
                <p class="text-xl-left">Contacto: {{ $someArray[0]["celularFamiliar"]}}</p>
                <p>Ultima Actualizacion: {{ $someArray[0]["fechaActualizacion"]}}</p>      
                   </div>           
        </div>
    </div>

    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="row">
            <div class="col-3">
                <a class="" href="{{ Route('programa.create')}}">Programas</a>
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <form action="/unidadAprendizaje/">

            <input type="hidden" name="username" value="{{ $someArray[0]['username']}}"> {{Session::put('usernameEst', $someArray[0]['username'])}}
            <!--<button type="submit" class="btn btn-primary">EditarNose sabe</button>-->
        </form>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">N°</th>
                    <th scope="col">Unidad de Aprendizaje</th>
                    <th scope="col">Nivel</th>
                    <th scope="col">Estado</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($asignatura as $a)

                <tr>
                    <td>{{ $loop->index+1}}</td>
                    <td scope="row"><a href="/cursoAuxiliar/{{$a->unidadAprendizaje_id}}">{{$a->nomUnidadAprendizaje }}</a></td>
                    <td>{{$a->nivel}}</td>
                    <td></td>

                    <!--<option value="{{--$a->id--}}">{{--$a->nomUnidadAprendizaje--}}Accion: <a href="#" id="xxx">{{--$a->id--}}</a></option>-->

                    <!--  <form action="/matricula/">
            Codigo programa:Dasha
            <input type="text" name="username" value="{{--$student->username--}}">
            <button type="submit" class="btn btn-primary">Buscar Nombre</button>
        </form>-->



                    {{-- @foreach ($programas as $pro)--}}

                </tr>
                @endforeach
            </tbody>
        </table>
        <!--
        <tr>
            <td scope="row">{{--$pro->codPrograma --}}</td>
            <td scope="row"><a href="/asignacionUnidad/{{--$pro->id--}}">{{--$pro->nomPrograma --}}</a></td>
            <td scope="row">{{--$pro->abreviatura --}}</td>
            <td><a href="/student/{{--$pro->id--}}" class="btn btn-primary">DetalleX-</a></td>-->
        <!--<td><a href="/student//edit" class="btn btn-warning">Editar</a></td>-->
        </tr>
        {{--@endforeach--}}


        <!--<a class="" href="{{-- Route('unidadAprendizaje.index')--}}">Unidades de Aprendizaje...</a>-->
    </div>
    <input type="hidden" value="{{ $someArray[0]['username']}}" id="username" name="username">


    <div class="tab-pane fade" id="record" role="tabpanel" aria-labelledby="contact-tab">
        <br>
        <a href="#" class="btn btn-info" id="mostrarNota">Consultar Notas</a>
        <!-- <div class="row">
            <div class="form-group col-4">
                <label for="exampleInputEmail1">Periodo Académico: </label>
                <select class="form-control" name="periodo_id" id="periodo_id">
                      {{-- @foreach ($periodo as $per)--}}  
                            <option value="{{-- $per->id --}}">{{--$per->nomPeriodo--}}</option>
                      {{--  @endforeach  --}}
                     </select>
                     <button id="consulta">Notas</button>
            </div>           
        </div>-->
        <hr>
        <div id="contenedor">..::..</div>
    </div>
</div>
</div>
@endsection