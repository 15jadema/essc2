@extends('layouts.plantilla') @section('title','Jornada') @section('content')
<!--
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/programa.js"></script>-->
<div class="container">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Crear Jornada</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/jornada" class="form-group" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="row">

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nombre Jornada</label>
                            <input type="text" class="form-control" name="nomJornada" id="nomJornada" placeholder="Nombre">

                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Estado</label>
                            <select class="form-control" name="estado_id" id="estado_id">
                             @foreach ($estado as $es)
                              <option value="{{$es->id}}">{{$es->nomEstado}}</option>
                              @endforeach  
                           </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>


        @endsection