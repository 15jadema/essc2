


@php
    use App\Enums\ETipoMovimiento;
    use App\Models\Recarga;
    use App\Models\Sucursal;
    use Illuminate\Support\Arr;
    use Carbon\Carbon;
    $suma = DB::table('sucursales')->where('codsucursal', '<>', 12)->select(DB::raw('SUM(saldo) as total'))->first();
    $consignaciones = DB::table('sucursal_consignaciones')->where('codsucursal', '<>', 12)->where('tipo', '=', 1)->where('estado', '=', 1)->select(DB::raw('SUM(valor) as total'))->first();
    $vlrTotal = 0;
    $nombreArchivo = 'Reporte Final';
    if(!blank(request('codsucursal'))){
        $sucursal = Sucursal::find(request('codsucursal'));
        $nombreArchivo.=' SUC-'.str_pad($sucursal->codsucursal, 4, "0",STR_PAD_LEFT).' '.$sucursal->nombres.' '.request('rango_fecha');
    }
    $listRecargasPaquetes  = [];

    foreach(DB::table('recargas_paquetes')->get() as $pac){
        $listRecargasPaquetes[$pac->codigo] = $pac;
    }
@endphp

@extends('layouts.administrador')

@section('title', trans('general.title_crear_pagina'))

@section('titulo-pagina')

<div id="page-title">

    <h1 class="page-header text-overflow">Reportes</h1>

</div>

@endsection

@section('breadcum')

<ol class="breadcrumb">

    <li><a href="#"><i class="demo-pli-home"></i></a></li>

    <li class="active">Administrador Reportes</li>

</ol>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-8">
        {!!Form::open(['route' => 'sucursal.cuadre-ganancia', 'id'=>'formBuscarEstado', 'method' => 'POST','enctype'=>'multipart/form-data']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sucursal</label>
                        <select class="default-select2 form-control select2-hidden-accessible" tabindex="-1" id="tipoSucursal" name="tipoSucursal" aria-hidden="true" data-placeholder="Seleccione un sucursal">
                            <optgroup label="Tipo de sucursal" >
                            <option value="">Seleccione tipo de sucursal</option>

                            @foreach(DB::table('sucursales AS s')->select('tipo_sucursal')->where('tipo_sucursal','<>',2)->groupBy('tipo_sucursal')->orderBy('tipo_sucursal')->get() as $tipo)
                                <option value="{{$tipo->tipo_sucursal}}" {{$tipo->tipo_sucursal==request('tipoSucursal') ? 'selected' : ''}}>
                                @php
                                    if($tipo->tipo_sucursal==1){
                                        echo "Comerciales";
                                    }else{
                                        echo "Movil AFA";
                                    }
                                @endphp
                                </option>
                            @endforeach

                            </optgroup>
                        </select>
                     </div>
                </div>
                <div id="sucursal-select" class="col-sm-4">
                    <div class="form-group">
                        <label>Sucursal</label>
                        <select class="default-select3 form-control select2-hidden-accessible" tabindex="-1" id="codsucursal" name="codsucursal" aria-hidden="true" data-placeholder="Seleccione un sucursal" disabled>
                        <optgroup label="Sucursales">
                        <option id="seleccionNullTipo" value="">Seleccione tipo de sucursal</option>
                        </optgroup>
                        </select>
                   </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>RANGO FECHA ESTADO</label>
                        <div class="input-group" id="default-daterange">
                            <input id="daterange" type="text" name="rango_fecha" class="form-control" value="{{request('rango_fecha')}}" placeholder="Especifique una fecha a buscar" autocomplete="false" />
                            <span class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                    <label>...</label>
                    <button type="submit" class="btn btn-primary">Buscar</button>
                </div>
            </div>
        {{Form::close()}}
        <div class="panel panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-vcenter mar-top" id="tablaDatatable">
                        <thead>
                            <tr>
                                <th class="min-w-td text-left">No.</th>
                                <th class="min-w-td text-left">Sucursal</th>
                                <th class="text-center">Fecha</th>
                                <th class="min-w-td">Tipo Movimiento</th>
                                <th class="min-w-td">No. Factura</th>
                                <th class="min-w-td text-center">Valor</th>
                                <th class="min-w-td text-center">Costo</th>
                                <th class="text-right">Comision</th>
                                <th class="min-w-td text-center">Ganancia</th>
                                <th class="text-right">Iva</th>
                                <th style="width: 10%;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $valorGanancia = 0;
                            $valorVentas = 0;
                            $valorPorcentaje = 0;
                            $valorComision = 0;
                            $codmovimiento = NULL;
                            $valoresTotales = [1=>0,2=>0, 3=>0,4=>0,5=>0, 5.5=>0,8=>0];
                            $valoresFletes = ["G1" => 0, "G2"=> 0, "IVA" => 0];
                            $listGanancias = [1=>0,2=>0,3=>0,4=>0,5=>0,6=>0, 7 => 0,8=>[]];
                            //cambio
                            $listCantidadTipos = [1=>0,2=>0,3=>0,4=>0,5=>0,6=>0, 7 => 0,8=>[]];
                            $sec=1;
                            $listTotales = [];
                            $valores = ["cantidad"=>0,"costo"=>0,"valor"=>0,"comision"=>0, "ganancia"=>0,"iva"=>0];
                        @endphp
                        @foreach($listG as $ventas)
                            @php
                                if(!Arr::exists($listTotales, Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_date')))){
                                    $listTotales[Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_date'))] = [];
                                }
                                if(!Arr::exists($listTotales[Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_date'))], $ventas->tipo_movimiento)){
                                    $listTotales[Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_date'))][$ventas->tipo_movimiento] = 0;
                                }
                                $listTotales[Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_date'))][$ventas->tipo_movimiento]+=$ventas->valor_real;
                                $valor_comision = $ventas->costo_envia;
                                $valor_comision = blank($ventas->porcentaje_envia) ? $valor_comision : $valor_comision+$ventas->valor_real*$ventas->porcentaje_envia/100;
                                $valor_comisionr = $ventas->costo_paga;
                                $valor_comisionr = blank($ventas->porcentaje_paga) ? $valor_comisionr : $valor_comisionr+$ventas->valor_real*$ventas->porcentaje_paga/100;

                                $valor_iva = $ventas->iva_costo*1000/100;
                                $ventas->iva_porcentaje = "1.".$ventas->iva_porcentaje;
                                $costo = $ventas->costo==0 ? ($ventas->porcentaje*$ventas->valor_real/100) : ($ventas->costo) + ($ventas->porcentaje*$ventas->valor_real/100);
                                $valor_iva = blank($ventas->iva_porcentaje) ? $valor_iva : ($costo-$valor_comision-$valor_comisionr)-($costo-$valor_comision-$valor_comisionr)/$ventas->iva_porcentaje;
                                $valorVentas+=$valor_iva;
                                $ganancia = 0;

                                if($ventas->tipo_movimiento == 1&& $ventas->codsucursal!=12){
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                    $valoresFletes["G1"]+=$costo;
                                    $valoresFletes["IVA"]+=$valor_iva;
                                    $ganancia = ($ventas->tipo_sucursal==3) ? $costo : $costo-$valor_iva-$valor_comision-$valor_comisionr;
                                    $listGanancias[1]+=$ganancia;
                                    $listCantidadTipos[1]++;
                                }else if($ventas->tipo_movimiento == 2&& $ventas->codsucursal!=12){
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                }else if($ventas->tipo_movimiento == 3&& $ventas->codsucursal!=12){
                                    $costo = 1500;
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                    $ganancia = ($ventas->tipo_sucursal==3) ? $costo : 945;
                                    $listGanancias[3]+=$ganancia;
                                    $listCantidadTipos[3]++;
                                    $valoresFletes["G2"]+=1500;
                                }else if($ventas->tipo_movimiento == 4){
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                    $codrecpaq = Arr::exists($listRecargasPaquetes, $ventas->codrecpaq) ? $listRecargasPaquetes[$ventas->codrecpaq] : new Recarga();
                                    if($codrecpaq->codigo == 1 || $codrecpaq->codpadre==1){
                                        $ganancia = $ventas->valor_real*0.06;
                                    }else{
                                        $ganancia = $ventas->valor_real*0.07;
                                    }
                                    $ganancia = $ganancia - $ventas->valor_comision;
                                    $listGanancias[4]+=$ganancia;
                                    $listCantidadTipos[4]++;
                                }else if($ventas->tipo_movimiento == 5 && $ventas->codsucursal!=12){
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                    $ganancia = ($ventas->tipo_sucursal==3) ? 1200 : 70 ;
                                    $listGanancias[5]+=$ganancia;
                                    $listCantidadTipos[5]++;
                                }else if($ventas->tipo_movimiento == 6){

                                    $ganancia = ($ventas->valor_real*10/100) - $ventas->valor_comision;
                                    $listGanancias[6]+=$ganancia;
                                    $listCantidadTipos[6]++;
                                }else if($ventas->tipo_movimiento == 7){

                                    $ganancia = ($ventas->tipo_sucursal==3) ? ($ventas->prima_soat*2.5/100)/5*3 :$ventas->valor_comision/5*3;
                                    $listGanancias[7]+=$ganancia;
                                    $listCantidadTipos[7]++;
                                }else if($ventas->tipo_movimiento == 8 && $ventas->codsucursal!=12){
                                    $costo=$ventas->v_tarifa;
                                    $valoresTotales[$ventas->tipo_movimiento]+=$ventas->valor_real;
                                    $ganancia = ($ventas->tipo_sucursal==3) ? $costo : $costo-$costo*0.37  ;
                                    if(array_key_exists($ventas->v_codconvenio,$listGanancias[8]) && array_key_exists($ventas->v_codconvenio,$listCantidadTipos[8])){
                                          $listGanancias[8][$ventas->v_codconvenio]+=$ganancia;
                                          $listCantidadTipos[8][$ventas->v_codconvenio]++;
                                    }else{
                                          $listGanancias[8][$ventas->v_codconvenio]=$ganancia;
                                          $listCantidadTipos[8][$ventas->v_codconvenio]=1;
                                    }

                                }


                                $valorGanancia+=$ganancia;


                                $valorPorcentaje+=$ventas->valor_real;
                                $comision = $valor_comision+$valor_comisionr;
                                $valores["valor"]+=$ventas->valor_real;
                                $valores["comision"]+=$ventas->valor_comision;
                                $valores["ganancia"]+=$ganancia;
                                $valores["iva"]+=$valor_iva;
                                $valores["costo"]+=$costo;
                                $valores["cantidad"]++;

                                // cambio
                                if($ventas->tipo_movimiento == 1 || $ventas->tipo_movimiento == 2){

                                }else if($ventas->tipo_movimiento == 3){
                                $consignacion =DB::table('consignacion')->where('codconsignacion','=',$ventas->codconsignacion)->first();
                                $codconvenio=(!empty($consignacion))?$consignacion->codconvenio:'';
                                 }
                                else if($ventas->tipo_movimiento == 4){
                                $recarga = App\Models\Recarga::find($ventas->codrecarga);
                                $operador=(!blank($recarga))? DB::table('recargas_paquetes')->where('codigo','=',$recarga->codrecpaq)->select('nombre as operador')->first():'';
                                }else if($ventas->tipo_movimiento == 5){
                                $pago=DB::table('pagos')->where('codpago','=',$ventas->codpago)->first();
                                $codconvenio=$pago->codconvenio;
                                }else if($ventas->tipo_movimiento==8){
                                $codconvenio=$ventas->v_codconvenio;
                                }

                                if(!empty($codconvenio)){
                                    $convenio =DB::table('convenios')->where('codconvenio','=',$codconvenio)->first();
                                }
                            @endphp
                            <tr>
                                <td>{{$ventas->codrel}}</td>
                                <td>
                                    {{-- cambio --}}
                                    <i>{{$ventas->municipio}}</i><br>
                                    {{$ventas->nombres}}
                                    <br><b>SUC-{{str_pad($ventas->codsucursal, 4, "0",STR_PAD_LEFT)}}</b>
                                </td>
                                <td class="text-right">{{Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_datetime'))}}</td>
                                <td>{!!ETipoMovimiento::result($ventas->tipo_movimiento)->getDescription()!!}<br>
                                {{-- cambio --}}
                                    @php
                                     if($ventas->tipo_movimiento == 1 || $ventas->tipo_movimiento == 2){

                                    }else if($ventas->tipo_movimiento==4){
                                     echo (!blank($operador)) ? strip_tags($operador->operador) : '' ;
                                    }else{
                                     echo (!blank($convenio)) ? strip_tags($convenio->nombre) : '' ;
                                    }
                                    @endphp
                                </td>
                                <td>
                                    @php
                                    $sec++;
                                        if($ventas->tipo_movimiento == 1 || $ventas->tipo_movimiento == 2){
                                            echo "V-".str_pad($ventas->codmovimiento, 4, "0",STR_PAD_LEFT);
                                        }else if($ventas->tipo_movimiento == 3){
                                            echo "VC-".str_pad($ventas->codconsignacion, 4, "0",STR_PAD_LEFT);
                                        }else if($ventas->tipo_movimiento == 4){
                                            echo "R-".str_pad($ventas->codrecarga, 4, "0",STR_PAD_LEFT);
                                        }else if($ventas->tipo_movimiento == 5){
                                            echo "P-".str_pad($ventas->codpago, 4, "0",STR_PAD_LEFT);
                                        }else if($ventas->tipo_movimiento == 6){
                                            echo "P-".str_pad($ventas->codpago, 4, "0",STR_PAD_LEFT);
                                        }else if($ventas->tipo_movimiento == 8){
                                            echo "RT-".str_pad($ventas->codretiro, 4, "0",STR_PAD_LEFT);
                                        }
                                    @endphp
                                </td>
                                <td> {{number_format($ventas->valor_real)}}</td>
                                <td> {{number_format($costo)}}</td>
                                <td> {{number_format($ventas->valor_comision)}}</td>
                                <td>
                                     {{$ventas->tipo_movimiento == 2 ? '0' : number_format($ganancia)}}
                                </td>
                                <td>
                                     {{$ventas->tipo_movimiento == 1 ? number_format($valor_iva) : '0'}}
                                </td>
                                <td>
                                    @if($ventas->tipo_movimiento==1)
                                    <a data-fancybox data-type="iframe" class="btn btn-purple btn-sm fa fa-search" data-src="{{url('factura').'?m='.$ventas->codmovimiento}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @elseif($ventas->tipo_movimiento==2)
                                    <a data-fancybox data-type="iframe" class="btn btn-dark btn-sm fa fa-search" data-src="{{url('factura-retira').'?m='.$ventas->codmovimiento}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @elseif($ventas->tipo_movimiento==3)
                                    <a data-fancybox data-type="iframe" class="btn btn-warning btn-sm fa fa-search" data-src="{{url('factura-consignacion').'?codconsignacion='.$ventas->codconsignacion}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @elseif($ventas->tipo_movimiento==4)
                                    <a data-fancybox data-type="iframe" class="btn btn-green btn-sm fa fa-search" data-src="{{url('factura-recarga').'?codrecarga='.$ventas->codrecarga}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @elseif($ventas->tipo_movimiento==5)
                                    <a data-fancybox data-type="iframe" class="btn btn-green btn-sm fa fa-search" data-src="{{url('factura-pago').'?codpago='.$ventas->codpago}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @elseif($ventas->tipo_movimiento==8 && $ventas->v_estado==3)
                                    <a data-fancybox data-type="iframe" class="btn btn-green btn-sm fa fa-search" data-src="{{url('factura-retiro').'?codretiro='.$ventas->codretiro}}"  href="javascript:;">
                                        <i class="ti ti-printer"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td>

                                </td>
                                <td class="text-right"></td>
                                <td>TOTALES</td>
                                <td>
                                    Cant. ({{number_format($valores["cantidad"])}})
                                </td>
                                <td> {{number_format($valores["valor"])}}</td>
                                <td> {{number_format($valores["costo"])}}</td>
                                <td> {{number_format($valores["comision"])}}</td>
                                <td> {{number_format($valores["ganancia"])}}</td>
                                <td> {{number_format($valores["iva"])}}</td>
                                <td>

                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
            <!-- begin card-body -->
            <div class="card-body" style="padding: 1rem !important;">
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-7 -->
                    <div class="col-xl-12">
                        <!-- begin title -->
                        <div class="text-grey">
                            <b>TOTAL GANANCIA</b>
                        </div>
                        <!-- end title -->
                        <!-- begin total-sales -->
                        <div class="d-flex mb-1">
                            <h2 class="mb-0 text-white">$ <span data-animation="number" data-value="64559.25">{{number_format($valorGanancia)}}</span></h2>
                        </div>
                        <!-- end total-sales -->
                        <hr class="bg-white-transparent-2">
                        <!-- begin row -->
                        <div class="row text-truncate">
                            @foreach($listGanancias as $tipo => $ganancia)
                                @if($tipo!=2 && $tipo!=8)
                                <!-- begin col-6 -->
                                <div class="col-6">
                                    <div class="f-s-12 text-grey">{!!ETipoMovimiento::result($tipo)->getDescription()!!} {{ $listCantidadTipos[$tipo]}}</div>
                                    <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">{{number_format($ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100)}}% ($ {{number_format($ganancia)}})</div>
                                    <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                        <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%" style="width: {{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%;"></div>
                                    </div>
                                </div>
                                <!-- end col-6 -->
                                @endif
                            @endforeach
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end col-7 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end card-body -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                    <!-- begin card-body -->
                    <div class="card-body" style="padding: 1rem !important;">
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-7 -->
                            <div class="col-xl-12">
                                <!-- begin title -->
                                <div class="text-grey">
                                    <b>Total giros</b>
                                </div>
                                <!-- end title -->
                                <!-- begin total-sales -->
                                <div class="d-flex mb-1">
                                    <h2 class="mb-0 text-white">$ <span data-animation="number" data-value="64559.25">{{number_format($valoresTotales[1]-$valoresTotales[2])}}</span></h2>
                                </div>
                                <!-- end total-sales -->
                                <hr class="bg-white-transparent-2">
                                <!-- begin row -->
                                <div class="row text-truncate">
                                    @foreach($valoresTotales as $tipo => $ganancia)
                                        @if($tipo<=2)
                                        <!-- begin col-6 -->
                                        <div class="col-6">
                                            <div class="f-s-12 text-grey">
                                                {!!ETipoMovimiento::result($tipo)->getDescription()!!}

                                            </div>
                                            <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">$ {{number_format($ganancia)}}
                                                @if($tipo==1)
                                                    <small>($ {{number_format($valoresFletes["G1"])}})</small>
                                                @endif
                                            </div>
                                            <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                                <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%" style="width: {{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%;"></div>
                                            </div>
                                        </div>
                                        <!-- end col-6 -->
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!-- end col-7 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                    <!-- begin card-body -->
                    <div class="card-body" style="padding: 1rem !important;">
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-7 -->
                            <div class="col-xl-12">
                                <!-- begin title -->
                                <div class="text-grey">
                                    <b>Total Retiros</b>
                                </div>
                                <!-- end title -->
                                <!-- begin total-sales -->
                                <div class="d-flex mb-1">
                                    <h2 class="mb-0 text-white">$ <span data-animation="number" data-value="64559.25">{{number_format(array_sum($listGanancias[8]))}}</span></h2>
                                </div>
                                <!-- end total-sales -->
                                <hr class="bg-white-transparent-2">
                                <!-- begin row -->
                                <div class="row text-truncate">
                                    @foreach($listGanancias[8] as $key=>$gan)
                                    @php
                                    $conv=DB::table('convenios')->where('codconvenio',$key)->first();
                                    $gananciaT=array_sum($listCantidadTipos[8]);
                                    $porcentaje=round($gananciaT/(($listGanancias[8][$key]>0 ? $listGanancias[8][$key] : 1)*100),2);
                                    @endphp
                                        <div class="col-6">
                                            <div class="f-s-12 text-grey">
                                            {{$conv->nombre}} {{$gananciaT}}
                                            </div>
                                            <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">{{$porcentaje}}%
                                                    <small>($ {{number_format($listGanancias[8][$key])}})</small>
                                            </div>
                                            <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                                <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$porcentaje}}%"
                                                style="width: {{$porcentaje}}%;"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- end col-7 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
            @foreach($valoresTotales as $tipo => $valores)
                @if($tipo>2)
                    @php
                        $vlrTotal+= $valores;
                    @endphp
                <div class="col-sm-6">
                    <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                        <!-- begin card-body -->
                        <div class="card-body" style="padding: 1rem !important;">
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-7 -->
                                <div class="col-xl-12">
                                    <!-- begin title -->
                                    <div class="text-grey">
                                        <b>{!!ETipoMovimiento::result($tipo)->getDescription()!!}</b>
                                    </div>
                                    <!-- end title -->
                                    <!-- begin total-sales -->
                                    <div class="d-flex mb-1">
                                        <h2 class="mb-0 text-white">
                                            $ <span data-animation="number" data-value="64559.25">{{number_format($valores)}}</span>
                                            @if($tipo==3)
                                                <small class="text-white">($ {{number_format($valoresFletes["G2"])}})</small>
                                            @endif
                                        </h2>
                                    </div>
                                    <!-- end total-sales -->
                                </div>
                                <!-- end col-7 -->
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end card-body -->
                    </div>
                </div>
                @endif
            @endforeach
            <div class="col-sm-6">
                <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                    <!-- begin card-body -->
                    <div class="card-body" style="padding: 1rem !important;">
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-7 -->
                            <div class="col-xl-12">
                                <!-- begin title -->
                                <div class="text-grey">
                                    <b>COMISIONES PAGADAS</b>
                                </div>
                                <!-- end title -->
                                <!-- begin total-sales -->
                                <div class="d-flex mb-1">
                                    <h2 class="mb-0 text-white">
                                        $ <span data-animation="number" data-value="64559.25">{{number_format($comisionesPagadas)}}</span>

                                    </h2>
                                </div>
                                <!-- end total-sales -->
                            </div>
                            <!-- end col-7 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                    <!-- begin card-body -->
                    <div class="card-body" style="padding: 1rem !important;">
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-7 -->
                            <div class="col-xl-12">
                                <!-- begin title -->
                                <div class="text-grey">
                                    <b>Total ingresos</b>
                                </div>
                                <!-- end title -->
                                <!-- begin total-sales -->
                                <div class="d-flex mb-1">
                                    <h2 class="mb-0 text-white">$ <span data-animation="number" data-value="64559.25">{{number_format($valoresFletes["G2"]+$valoresFletes["G1"]+$valoresTotales[1]+$valoresTotales[3]+$valoresTotales[4]+$valoresTotales[5]-$valoresTotales[2]-$suma->total-$consignaciones->total)}}</span></h2>
                                </div>
                                <!-- end total-sales -->
                                <hr class="bg-white-transparent-2">
                                <!-- begin row -->
                                <div class="row text-truncate">
                                    <!-- begin col-6 -->
                                    <div class="col-12">

                                        <div class="f-s-12 text-grey">Total Ingresos</div>
                                        <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">$ {{number_format($valoresFletes["G2"]+$valoresFletes["G1"]+$valoresTotales[1]+$valoresTotales[3]+$valoresTotales[4]+$valoresTotales[5]-$valoresTotales[2])}}</div>
                                        <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                            <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%" style="width: {{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%;"></div>
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                    <!-- begin col-6 -->
                                    <div class="col-12">
                                        <div class="f-s-12 text-grey">Saldos Sucursales</div>
                                        <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">$ {{number_format($suma->total)}}</div>
                                        <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                            <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%" style="width: {{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%;"></div>
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                    <!-- begin col-6 -->
                                    <div class="col-12">
                                        <div class="f-s-12 text-grey">Consignaciones Sucursales</div>
                                        <div class="f-s-18 m-b-5 f-w-600 p-b-1" data-animation="number" data-value="1568">$ {{number_format($consignaciones->total)}}</div>
                                        <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                            <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="{{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%" style="width: {{$ganancia/($valorGanancia>0 ? $valorGanancia : 1)*100}}%;"></div>
                                        </div>
                                    </div>
                                    <!-- end col-6 -->
                                </div>
                            </div>
                            <!-- end col-7 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
                    <!-- begin card-body -->
                    <div class="card-body" style="padding: 1rem !important;">
                        <!-- begin row -->
                        <div class="row">
                            <!-- begin col-7 -->
                            <div class="col-xl-12">
                                <!-- begin title -->
                                <div class="text-grey">
                                    <b>IVA</b>
                                </div>
                                <!-- end title -->
                                <!-- begin total-sales -->
                                <div class="d-flex mb-1">
                                    <h2 class="mb-0 text-white">
                                        $ <span data-animation="number" data-value="64559.25">{{number_format($valoresFletes["IVA"])}}</span>
                                    </h2>
                                </div>
                                <!-- end total-sales -->
                            </div>
                            <!-- end col-7 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-body">
            <canvas id="myChart"></canvas>
        </div>
    </div>
    @if(1==2)
    <div class="col-lg-12">

        <div class="panel panel-body">
            <div class="panel-body" id="tableReportes">
                <div class="row">
                    @if(\Auth::user()->email!=1)
                        <div class="col-sm-9">
                            <table class="table table-vcenter mar-top" id="tablaDatatable">

                                <thead>

                                    <tr>

                                        <th class="min-w-td">No. Factura</th>
                                        <th class="min-w-td text-left">Sucursal</th>
                                        <th class="min-w-td text-center">Valor</th>
                                        <th class="text-right">Tarifa</th>
                                        <th class="min-w-td text-center">Costo</th>
                                        <th class="text-right">Iva</th>
                                        <th class="text-right">Gan. Suc.</th>
                                        <th class="text-right">Ganancia</th>
                                        <th class="text-center">Fecha</th>
                                        <th style="width: 10%;"></th>

                                    </tr>

                                </thead>

                                <tbody>

                                @php

                                    $valorGanancia = 0;
                                    $valorVentas = 0;
                                    $valorPorcentaje = 0;
                                    $valorComision = 0;
                                    $codmovimiento = NULL;
                                @endphp

                                @foreach(DB::table('rel_sucursal_movimiento AS r')->join('movimientos AS m', 'm.codmovimiento', '=', 'r.codmovimiento')->join('tarifas AS t','t.codtarifa','=','r.codtarifa')->join('sucursales AS s','s.codsucursal','=','r.codsucursal')->select('r.*','t.*','s.nombres AS nombreSucursal', 's.ubicacion', 'm.estado')->get() as $ventas)
                                    @if($ventas->codmovimiento!=$codmovimiento)
                                    @php
                                        $valor_comision = $ventas->costo;
                                        $ganancia = blank($ventas->porcentaje) ? $valor_comision : $valor_comision+$ventas->valor_real*$ventas->porcentaje/100;

                                        $valor_comision = $ventas->costo_envia;
                                        $valor_comision = blank($ventas->porcentaje_envia) ? $valor_comision : $valor_comision+$ventas->valor_real*$ventas->porcentaje_envia/100;
                                        $valor_comisionr = $ventas->costo_paga;
                                        $valor_comisionr = blank($ventas->porcentaje_paga) ? $valor_comisionr : $valor_comisionr+$ventas->valor_real*$ventas->porcentaje_paga/100;
                                        $valor_iva = $ventas->iva_costo*1000/100;
                                        $ventas->iva_porcentaje = "1.".$ventas->iva_porcentaje;
                                        $costo = $ventas->costo==0 ? ($ventas->porcentaje*$ventas->valor_real/100) : ($ventas->costo) + ($ventas->porcentaje*$ventas->valor_real/100);
                                        $valor_iva = blank($ventas->iva_porcentaje) ? $valor_iva : ($costo-$valor_comision-$valor_comisionr)-($costo-$valor_comision-$valor_comisionr)/$ventas->iva_porcentaje;
                                        $valorPorcentaje+= $valor_iva;
                                        $valorVentas+=$ventas->valor_real;
                                        $comision = $valor_comision+$valor_comisionr;
                                        $valorComision+=$comision;
                                        $valorGanancia+=$ganancia-$comision-$valor_iva;
                                    @endphp
                                        <tr>
                                            <td class="min-w-td">FV-000{{$ventas->codmovimiento}}</td>
                                            <td class="text-left"> {{($ventas->nombreSucursal)}} - {{($ventas->ubicacion)}}  </td>
                                            <td class="text-right">$ {{number_format($ventas->valor_real)}} </td>
                                            <td class="text-right">
                                                $ {{number_format($ventas->valor_inicial,0)}} - $ {{number_format($ventas->valor_final,0)}}
                                            </td>
                                            <td class="text-right">
                                                $ {{number_format($ganancia,0)}}
                                            </td>
                                            <td class="text-right">
                                                $ {{number_format($valor_iva)}}
                                            </td>
                                            <td class="text-right">
                                                $ {{number_format($comision,0)}}
                                            </td>
                                            <td class="text-right">
                                                $ {{number_format($ganancia-$comision-$valor_iva,0)}}
                                            </td>
                                            <td class="text-right">{{Carbon::parse($ventas->fecha_movimiento)->format(trans('general.format_datetime'))}}</td>
                                            <td class="text-center" style="width: 10%;">
                                                <a data-fancybox data-type="iframe" class="btn btn-purple btn-sm fa fa-search" data-src="{{url('factura').'?m='.$ventas->codmovimiento}}"  href="javascript:;">
                                                    <i class="ti ti-printer"></i>
                                                </a>
                                                @if($ventas->estado==1)
                                                <a data-fancybox data-type="iframe" class="btn btn-dark btn-sm fa fa-search" data-src="{{url('factura-retira').'?m='.$ventas->codmovimiento}}"  href="javascript:;">
                                                    <i class="ti ti-printer"></i>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                    @php
                                        $codmovimiento = $ventas->codmovimiento;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <div class="col-lg-12">
                                <div class="widget widget-stats bg-green">
                                    <div class="stats-icon"><i class="fa fa-dollar-sign"></i></div>
                                    <div class="stats-info">
                                        <h4>Valor movimientos</h4>
                                        <p id="valor-iva">$ {{number_format($valorVentas)}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="widget widget-stats bg-warning">
                                    <div class="stats-icon"><i class="fa fa-dollar-sign"></i></div>
                                    <div class="stats-info">
                                        <h4>IVA</h4>
                                        <p id="valor-iva">$ {{number_format($valorPorcentaje)}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="widget widget-stats bg-danger">
                                    <div class="stats-icon"><i class="fa fa-dollar-sign"></i></div>
                                    <div class="stats-info">
                                        <h4>Valor comisi車n</h4>
                                        <p id="valor-iva">$ {{number_format($valorComision)}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="widget widget-stats bg-primary">
                                    <div class="stats-icon"><i class="fa fa-dollar-sign"></i></div>
                                    <div class="stats-info">
                                        <h4>Valor ganancia</h4>
                                        <p id="valor-iva">$ {{number_format($valorGanancia)}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif
    @php
        $listTipos = [];
        if(count($listTotales)>0){
            foreach($listTotales as $tipo => $ventas){

            foreach(ETipoMovimiento::data() as $tipo){
                if(Arr::exists($ventas, $tipo->getId())){
                    $listTipos[$tipo->getId()][] = ($ventas[$tipo->getId()]);
                }else{
                    $listTipos[$tipo->getId()][] = 0;
                }
            }
        }
        }else{
            foreach(ETipoMovimiento::data() as $tipo){
                $listTipos[$tipo->getId()][] = 0;
            }
        }
        $generales=DB::table('sucursales')->orderBy('codsucursal')->where('bloqueo', '<>', 1)->where('tipo_sucursal',1)->get();
        $personalizadas=DB::table('sucursales')->orderBy('codsucursal')->where('bloqueo', '<>', 1)->where('tipo_sucursal',3)->get();
        //print_r($listTipos);
    @endphp
</div>

@endsection

@section('script')

<script>

@if(!blank(request("codsucursal")))

    var html='<div class="form-group">'
                        +'<label>Sucursal</label>'
                        +'<select class="default-select3 form-control select2-hidden-accessible" tabindex="-1" id="codsucursal" name="codsucursal" aria-hidden="true" data-placeholder="Seleccione un sucursal">'
                        +  '<optgroup label="Sucursales">'
                        +  '<option id="seleccionNullTipo" value="">Seleccione sucursal</option>';
     if($('#tipoSucursal').val()==1){
     @foreach($generales as $general)
      html+='<option  value="{{$general->codsucursal}}" {{$general->codsucursal==request("codsucursal") ? "selected" : ""}}>{{$general->nombres}} {{$general->ubicacion}}</option>';
      @endforeach

     }else if($('#tipoSucursal').val()==3){

     @foreach($personalizadas as $personal)
      html+='<option  value="{{$personal->codsucursal}}" {{$personal->codsucursal==request("codsucursal") ? "selected" : ""}}>{{$personal->nombres}} {{$personal->ubicacion}}</option>';
      @endforeach

     }

     html+='</optgroup>'
           +'</select>'
           + '</div>';

     $('#sucursal-select').html(html);
     $('#codsucursal').chosen();
@endif

$('#tipoSucursal').on('change',function(event){
  var html='<div class="form-group">'
                        +'<label>Sucursal</label>'
                        +'<select class="default-select3 form-control select2-hidden-accessible" tabindex="-1" id="codsucursal" name="codsucursal" aria-hidden="true" data-placeholder="Seleccione un sucursal" disabled>'
                        +  '<optgroup label="Sucursales">'
                        +  '<option id="seleccionNullTipo" value="">Seleccione tipo de sucursal</option>'
                        +'</optgroup>'
                        +'</select>'
                   + '</div>' ;

 $('#codsucursal option').each(function(){
   if($(this).attr("id") == "seleccionNullTipo"){

   }else{
       $(this).remove();
   }

 });



if($(this).val()!=''){

    var html='<div class="form-group">'
                        +'<label>Sucursal</label>'
                        +'<select class="default-select3 form-control select2-hidden-accessible" tabindex="-1" id="codsucursal" name="codsucursal" aria-hidden="true" data-placeholder="Seleccione un sucursal">'
                        +  '<optgroup label="Sucursales">'
                        +  '<option id="seleccionNullTipo" value="">Seleccione sucursal</option>';

     if($(this).val()==1){
     @foreach($generales as $general)
      html+='<option  value="{{$general->codsucursal}}" {{$general->codsucursal==request("codsucursal") ? "selected" : ""}}>{{$general->nombres}} {{$general->ubicacion}}</option>';
      @endforeach

     }else if($(this).val()==3){

     @foreach($personalizadas as $personal)
      html+='<option  value="{{$personal->codsucursal}}" {{$personal->codsucursal==request("codsucursal") ? "selected" : ""}}>{{$personal->nombres}} {{$personal->ubicacion}}</option>';
      @endforeach

     }

     html+='</optgroup>'
           +'</select>'
           + '</div>';

     $('#sucursal-select').html(html);
     $('#codsucursal').chosen();

}else{
     $('#sucursal-select').html(html);
}

});

 var ctx = document.getElementById('myChart').getContext('2d');
 var myChart = new Chart(ctx, {
     type: 'line',
     data: {

         labels: ['{!!implode("','",array_keys($listTotales))!!}'],

         datasets: [
            @foreach(ETipoMovimiento::data() as $tipo)
            {
                label: '{!!strip_tags($tipo->getDescription())!!}',
                data: [{{implode(',',$listTipos[$tipo->getId()])}}],
                backgroundColor: [
                    'rgba({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}}, 0.2)',
                ],
                borderColor: [
                    'rgba(0, 99, 132, 1)',
                ],
                borderWidth: 1
            },
            @endforeach
         ],
     },
     options: {
         scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero: true
                 }
             }]
         }
     }
 });
$('[data-fancybox]').fancybox({

    toolbar  : false,

    smallBtn : true,

    iframe : {

        preload : false

    }

});

$('.btnConsultar').click(function(){

    frameworkApp.setLoadData({

        url: '{{ url("producto/informe-generar") }}',

        data: {

            codproducto: $("#codproducto").val(),

            codvendedor: $("#codvendedor").val(),

            inicio: $("#start").val(),

            fin: $("#end").val()

        },

        id_container_body: false,

        success: function(data) {

            console.log(data);

            if(data.error==0){

                $("#tableReportes").html(data.tablaVentas);

            }else{

                frameworkApp.setToastError(data.mensaje);

            }

        }

    });

});

$('#demo-dp-range .input-daterange').datepicker({

    format: "dd/mm/yyyy",

    todayBtn: "linked",

    language: 'es',

    autoclose: true,

    todayHighlight: true

});


var handleDataTableButtons=function(){"use strict";0!==$(".table-vcenter").length&&$(".table-vcenter").DataTable(
    {
        dom:"Bfrtip",
        "pageLength": 25,
        buttons:[
            {extend:"copy",className:"btn-sm"},
            {extend:"excel",title: '{{$nombreArchivo}}', className:"btn-sm"},
            {extend:"pdf",className:"btn-sm"},
            {extend:"print",className:"btn-sm"}
            ],
            responsive:!0

    }
    )},
    TableManageButtons=function(){"use strict";return{init:function(){handleDataTableButtons()}}}();
TableManageButtons.init();

$('.btn-evidencias').click(function(){

    var codasignatura = $(this).data("codasignatura");

    $("#codasignaturaEvidencias").val(codasignatura);

    $("#tablaEvidencias").html('');

    frameworkApp.setLoadData({

        url: '{{ url("evidencias") }}',

        data: {

            codasignatura: codasignatura,

        },

        id_container_body: false,

        success: function(data) {

            console.log(data);

            $("#tablaEvidencias").html(data.html);

            $("#modalEditar").modal('show');

            //frameworkApp.setToastSuccess(data.mensaje);

        }

    });

});
$('#daterange').daterangepicker({
    locale: {
    format: 'DD/MM/YYYY'
    },

    todayBtn: "linked",

    language: 'es',

    autoclose: true,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 días': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 días': [moment().subtract(29, 'days'), moment()],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },

    todayHighlight: true
});
$('.btn-editar').click(function(){

    var codasignatura = $(this).data("codasignatura");

    frameworkApp.setLoadData({

        url: '{{ url("asignatura/guardar-asignatura") }}',

        data: {

            codasignatura: codasignatura,

            nombreasignatura: $("#nombre"+codasignatura).val(),

            lema: $("#lema"+codasignatura).val(),

            ih: $("#ih"+codasignatura).val(),

            coddocente: $("#coddocente"+codasignatura).val(),

            coddocenteaux: $("#coddocenteAux"+codasignatura).val()

        },

        id_container_body: false,

        success: function(data) {

            frameworkApp.setToastSuccess(data.mensaje);

        }

    });


});
$(".default-select2").chosen();
function recargar(){

    location.reload();

}

</script>



@endsection
