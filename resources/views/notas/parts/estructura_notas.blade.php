<div id="tabExcel"></div>
<p id="log" class="alert alert-success"></p>
<script>
$(document).ready(function() {

    var changed = function(instance, cell, x, y, value) {
    var cellName = jexcel.getColumnNameFromId([x,y]);
    $('#log').text('');
    $('#log').text('Nuevo cambio en la celda ' +x+''+y+' valor '+cellName+'');

    var datos=table.getRowData(y);
    console.log(Object.assign({},datos));
    var link='{{url("api/notas/actualizar")}}/'+datos[2]+'/'+"{{$curso_id}}";
    $.ajax({
      url: link,
      type: 'post',
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
        data: {
          valores:datos,
        },
        success: function(){
               console.log('Datos actualizados');
        },
        error: function(){
                console.log('Datos no se pudieron actualizar');
        }
    });

}

var beforeChange = function(instance, cell, x, y, value) {
    var cellName = jexcel.getColumnNameFromId([x,y]);
    $('#log').text('');
    $('#log').text('La celda ' + cellName + 'sera cambiada');
}

var insertedRow = function(instance, cell) {
    $('#log').text('');
    $('#log').text('Fila Agregada');
}

var insertedColumn = function(instance, cell, x, y) {
    $('#log').text('');
    $('#log').text('Columna Agregada');

}
// var deletedRow = function(instance,cell) {
//     $('#log').text('');
//     $('#log').text('Fila Eliminada');
//     console.log(table.getRowData(cell));
//      var datos=table.getRowData(cell);
//      var link='{{url("api/notas/eliminar")}}/'+datos[3]+'/'+datos[1]+'/'+datos[3];
//     $.ajax({
//           url: link,
//           type: 'post',
//       headers: {
//       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//       },
//         success: function(data){
//                console.log('Dato eliminado');
//                return true;
//         },
//         error: function(){
//                 console.log('Dato no se pudo eliminar');
//                 return false;
//         }
//     });

// }

var deletedColumn = function(instance) {
    $('#log').text('');
    $('#log').text('Columna eliminada');
}

var sort = function(instance, cellNum, order) {
    var order = (order) ? 'desc' : 'asc';
    $('#log').text('');
    $('#log').text('La columna  ' + cellNum + ' se ordeno ' + order + '');
}

var resizeColumn = function(instance, cell, width) {
    $('#log').text('');
    $('#log').text('La columna  ' + cell + ' cambio el ancho ' + width + ' px');
}

var resizeRow = function(instance, cell, height) {
    $('#log').text('');
    $('#log').text('La fila  ' + cell + ' cambio el alto ' + height + ' px');
}

var selectionActive = function(instance, x1, y1, x2, y2, origin) {
    var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
    var cellName2 = jexcel.getColumnNameFromId([x2, y2]);
    $('#log').text('');
    $('#log').text('La selección de  ' + cellName1 + ' a ' + cellName2 + '');
}

var loaded = function(instance) {
    $('#log').text('');
    $('#log').text('Nuevos datos cargados');
}

var moveRow = function(instance, from, to) {
    $('#log').text('');
    $('#log').text('La fila ' + from + ' fue movida a la posición ' + to + ' ');
}

var moveColumn = function(instance, from, to) {
    $('#log').text('');
    $('#log').text('La col ' + from + ' fue movida a la posición ' + to + ' ');
}

var blur = function(instance) {
    $('#log').text('');
    $('#log').text('La tabla ' + $(instance).prop('id') + ' es difuminada');
}

var focus = function(instance) {
    $('#log').text('');
    $('#log').text('La tabla ' + $(instance).prop('id') + ' es seleccionada');
}

var table='';
$.ajax({
    url: "{{url('api/notas/getAll')}}/"+"{{$curso_id}}" ,
    success: function(data){
      var datos=data;
     console.log(datos);

table=jexcel(document.getElementById('tabExcel'), {
    data:datos[0],
    lazyLoading:true,
    loadingSpin:true,
    defaultColWidth: 100,
    tableOverflow: true,
    tableWidth: "48rem",
    columns: datos[1],
    nestedHeaders:datos[2],
    columnDrag:true,
    allowInsertRow:false,
    allowRenameColumn:false,
    allowDeleteColumn:false,
    onbeforeinsertcolumn:false,
    ondeletecolumn:false,
    onchange:changed,
    onbeforechange: beforeChange,
    oninsertrow: insertedRow,
    oninsertcolumn: insertedColumn,
    //onbeforedeleterow: deletedRow,
    ondeletecolumn: deletedColumn,
    onselection: selectionActive,
    onsort: sort,
    onresizerow: resizeRow,
    onresizecolumn: resizeColumn,
    onmoverow: moveRow,
    onmovecolumn: moveColumn,
    onload: loaded,
    onblur: blur,
    onfocus: focus,
        text:{

  areYouSureToDeleteTheSelectedRows:'Estas seguro de eliminar estas filas?',

},


});

    },
    error:function(){
        console.log('error al cargar los datos')
    }

});

});
</script>
</html>
