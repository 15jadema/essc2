<!DOCTYPE html>

<html lang="en">

<head>

    @if (Auth::user()->sede_id==1) {{Session::put('sede_id', 1)}} @else {{Session::put('sede_id', 2)}} @endif

    <title>Escuela de Salud Sur Colombiana</title>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">





    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>



    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <script src="../../js/script.js"></script>

</head>



<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <a class="navbar-brand" href="#">ESSC</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

<span class="navbar-toggler-icon"></span>

</button>



        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto">

                @if(Auth::user()->levelUser_id ==1)



                <form class="form-inline my-2 my-lg-0" method="GET" action="{{ Route('buscar')}}">

                    <input class="form-control mr-sm-2" type="search" placeholder="Buscar Estudiante" aria-label="Search" id="search" name="search">

                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>

                </form>

                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

Estudiantes

</a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ Route('student.index')}}">Estudiantes</a>

                        <a class="dropdown-item" href="{{ Route('docente.index')}}">Docentes</a>

                        <!--<a class="dropdown-item" href="{{ Route('administrador.index')}}">Administrador</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#">Institucional</a>-->

                    </div>

                </li>

                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

Académico

</a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ Route('calificaciones.index')}}">Evaluación de Aprendizaje</a>

                        <!--<a class="dropdown-item" href="{{ Route('docente.create')}}">Evaluación Asignaturas Individuales</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#">Otras Configuraciones</a>-->

                    </div>

                </li>



                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

Estructuración

</a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ Route('estructura.index')}}">Unidades de Aprendizaje</a>

                        <a class="dropdown-item" href="{{ Route('programa.index')}}">Programas</a>

                        <a class="dropdown-item" href="{{ Route('periodo.index')}}">Periodos</a>

                        <a class="dropdown-item" href="{{ Route('sede.create')}}">Sedes</a>

                        <a class="dropdown-item" href="/cursosX/{{Session::get('sede_id')}}">Cursos</a>

                        <a class="dropdown-item" href="{{ Route('docente.create')}}">Cerrar Cursos</a>

                        <a class="dropdown-item" href="{{ Route('jornada.create')}}">Crear Jornada</a>

                    </div>

                </li>



                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                            {{ Auth::user()->nombre }} {{ Auth::user()->nombre2 }} {{ Auth::user()->apellido }}   <span class="caret"></span>

                  

                    </a>



                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();

                                             document.getElementById('logout-form').submit();">

                                {{ __('Cerrar Sesión') }}

                            </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                            @csrf

                        </form>

                    </div>

                </li>



                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>



                        <input type="hidden" value="{{Session::get('sede_id')}}" id="sede_id_sesion">




                    </a>

                </li>

                <!-- <input type="hidden" value="{{--Auth::user()->sede_id --}}" id="sede_id">-->





                @endif @if(Auth::user()->levelUser_id ==2)

                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

    Matriculas

    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="#">Matriculas individuales</a>

                        <a class="dropdown-item" href="#">Matriculas en masa</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#">Something else here</a>

                    </div>

                </li>



                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                            {{ Auth::user()->nombres.text-uppercase }}  {{ Auth::user()->apellidos }}   {{ Auth::user()->sede_id }} <span class="caret"></span>

                        </a>



                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();

                                             document.getElementById('logout-form').submit();">

                                {{ __('Cerrar Sesión') }}

                            </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                            @csrf

                        </form>

                    </div>

                </li>

                @endif @if(Auth::user()->levelUser_id ==3)

                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

    Matriculas

    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="#">Matriculas individuales</a>

                        <a class="dropdown-item" href="#">Matriculas en masa</a>

                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#">Something else here</a>

                    </div>

                </li>



                <li class="nav-item dropdown">

                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                            {{ Auth::user()->nombres.text-uppercase }}  {{ Auth::user()->apellidos }}  <span class="caret"></span>

                        </a>



                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();

                                             document.getElementById('logout-form').submit();">

                                {{ __('Cerrar Sesión') }}

                            </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                            @csrf

                        </form>

                    </div>

                </li>

                @endif
            </ul>
            @if (Auth::user()->sede_id==1) Sede:

            <b> Pasto (Principal)</b> @else

            <b> Ipiales</b> @endif
        </div>

    </nav>

    <div class="container">
        @yield('content')
    </div>

    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>



</html>