@extends('layouts.plantilla') @section('title','Editar Estudiante') @section('content')
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>

<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Estudiante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>

            <form id="delete" action="/student/{{$student->id}}" class="form-group" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }} @method('DELETE') Realmente desea <strong class="text text-danger">eliminar</strong> al estudiante: <b> {{$student->apellidos}}  {{$student->nombres}} </b> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Información Estudiante</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/student/{{$student->id}}" class="form-group" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }} @method('PUT') @csrf
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Primer Apellido</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos Estudiante" value="{{$student->apellido}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Segundo Apellido</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{$student->apellido2}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Primer nombre</label>
                            <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombres Estudiante" value="{{$student->nombre}}">

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Segundo Apellido</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" {{$student->nombre2}}>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Tipo Documento</label>
                            <select class="form-control" name="documento_id" id="documento_id"> 
                                
                            </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Documento</label>
                            <input type="number" class="form-control" name="username" id="username" placeholder="Documento" value="{{$student->username}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="">Sexo</label>
                            <input type="hidden" id="genero_id_text" value="{{$student->genero_id}}" />
                            <select class="form-control" name="genero_id" id="genero_id">  

                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Correo Electrónico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{$student->email}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Teléfono</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{$student->telefono}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Celular</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{$student->celular}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                            <input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento" value="{{$student->fechaNacimiento}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Lugar Departamento</label>
                            <select class="form-control" name="depaNacimiento_id" id="depaNacimiento_id">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Dirección</label>
                            <select class="form-control" name="munNacimiento_id" id="munNacimiento_id" placeholder="Selecciona Municipio">
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Lugar de Recidencia</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Barrio</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Periodo</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Sede</label>
                            <select class="form-control" name="sede_id" id="sede_id">
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Programa</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <label for="foto">Foto</label>
                                <input type="file" class="form-control" name="foto">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Actualizar</button>
                </form>
                <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>
            </div>
        </div>
    </div>

    @endsection