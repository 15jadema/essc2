@extends('layouts.plantilla')
@section('title','Menu Listado')

@section('content')
<div class="container">
        <p>Periodos</p>
<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('periodo.create')}}" >Registrar Periodo</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre Periodo</th>
      <th scope="col">Ordenamiento</th>  
       <th scope="col">Estado</th>    
      <th scope="col">Fecha de Inicio</th>
      <th scope="col">Fecha de Fin</th>     
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($periodo as $per)
    <tr>
      <!--<td><img class="foto" src="images/"></td>-->
      <td scope="row">{{$per->nomPeriodo }}</td>
      <td scope="row">{{$per->ordenamiento }}</td>
      <td scope="row">{{$per->estado_id }}</td>
      <td scope="row">{{$per->fechaInicio }}</td>
      <td scope="row">{{$per->fechaFin }}</td>
      <td><a href="/student/{{$per->id}}" class="btn btn-primary">Detalle</a></td>
      <!--<td><a href="/student//edit" class="btn btn-warning">Editar</a></td>-->
    </tr>
  @endforeach
  </tbody>
</table>
{!! $periodo->render() !!}
</div>
@endsection

