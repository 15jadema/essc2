@extends('layouts.plantilla') @section('title','Estudiante detalle') @section('content')


<div class="container mt-5">
    Programa: {{$programa->nomPrograma}} <a href="{{ Route('asignacionUnidad.create')}}">
  Agregar Unidad de Aprendizaje</a>
    </button> @foreach ($asignatura as $asig)

    <h5>{{$asig->nomUnidadAprendizaje }} {{$asig->nivel }}</h5>

    @endforeach


</div>
@endsection

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Unidad de Aprendizaje</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <form action="/asignacionUnidad" class="form-group" method="POST">
                    <input type="text" value="{{$asig->id}}" id="programa_id">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Unidad de Aprendizaje</label>
                        <select class="form-control" id="unidadAprendizaje_id">
    @foreach ($unidadAprendizaje as $uni)
    
      <option value="{{$uni->id}}">{{$uni->nomUnidadAprendizaje }}</option>
   @endforeach
    </select>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label for="exampleInputPassword1">Nivel</label> @foreach ($unidadAprendizaje as $uni)

                            <option value="{{$uni->id}}">{{$uni->nomUnidadAprendizaje }}</option>
                            @endforeach
                        </div>

                        <div class="col-6">
                            <label for="exampleInputPassword1">Creditos</label>
                            <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Creditos">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-6">
                            <label for="exampleInputPassword1">I Hora Semana</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nivel" name="ihs" id="ihs">
                        </div>

                        <div class="col-6">
                            <label for="exampleInputPassword1">I Hora Total</label>
                            <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Creditos" name="iht" id="iht">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-6">
                            <label for="exampleInputPassword1">Evaluar Para Cerrar</label>
                            <select class="form-control" name="cerrar" id="cerrar">
                              <option value="1">Si</option>
                              <option value="2">No</option>
                              </select>
                        </div>
                    </div>
                    <input type="submit" value="Enviar">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Asignar</button>
            </div>
            </form>
        </div>
    </div>
</div>