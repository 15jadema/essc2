@extends('layouts.plantilla') @section('title','Estudiante detalle') @section('content')


<div class="container mt-5">
    Programa: <h3>{{$programa->nomPrograma}}</h3> <a href="">
  
<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('asignacionUnidad.create')}}" >Asingar Unidad de Aprendizaje</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">N°</th>
      <th scope="col">Unidad de Aprendizaje</th>      
      <th scope="col">Nivel</th>
      <th scope="col">Detalle</th>      
    </tr>
  </thead>
  <tbody>
  {{--dd($asignatura)--}}

     @foreach ($asignatura as $asig)
      <tr>
       <td>{{$loop->index+1}}</td>
      <td>{{$asig->nomUnidadAprendizaje }} </td><td>{{$asig->codigo }}</td>
      <td><button class="btn btn-primary">Detalle</button></td>
     </tr>
    @endforeach    
 </tbody>
</table>
    
  {{Session::put('grupo', $programa->id)}} 
<input type="hidden" name="miPrograma" value="{{$programa->id}}">
    

  


</div>
@endsection

<!-- Modal -->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Unidad de Aprendizaje.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <form action="/asignacionUnidad" class="form-group" method="POST">
                    <input type="text" value="" id="programa_id">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Unidad de Aprendizaje</label>
                        <select class="form-control" id="unidadAprendizaje_id">
   
    </select>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label for="exampleInputPassword1">Nivel</label>
                        </div>

                        <div class="col-6">
                            <label for="exampleInputPassword1">Creditos</label>
                            <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Creditos">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-6">
                            <label for="exampleInputPassword1">I Hora Semana</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nivel" name="ihs" id="ihs">
                        </div>

                        <div class="col-6">
                            <label for="exampleInputPassword1">I Hora Total</label>
                            <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Creditos" name="iht" id="iht">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-6">
                            <label for="exampleInputPassword1">Evaluar Para Cerrar</label>
                            <select class="form-control" name="cerrar" id="cerrar">
                              <option value="1">Si</option>
                              <option value="2">No</option>
                              </select>
                        </div>
                    </div>
                    <input type="submit" value="Enviar">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Asignar</button>
            </div>
            </form>
        </div>
    </div>
</div>-->