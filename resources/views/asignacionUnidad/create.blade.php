@extends('layouts.plantilla') @section('title','Menu de Estructura') @section('content')
<!--
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/programa.js"></script>-->


<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Asignar Unidad de Aprendizaje</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/asignacionUnidad" class="form-group" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="{{ Session::get('grupo') }}" name="programa_id"> @csrf
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="exampleInputEmail1">Unidad de Aprendizaje</label>
                            <select class="form-control" id="unidadAprendizaje_id" name="unidadAprendizaje_id">
                                @foreach ($unidadAprendizaje as $uni)                                
                                  <option value="{{$uni->id}}">{{$uni->nomUnidadAprendizaje }}</option>
                               @endforeach
                                </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nivel</label>
                            <select class="form-control" id="nivel" name="nivel">  
                                    @foreach ($nivel as $n)                                
                                    <option value="{{$n->id}}">{{$n->nombre }}</option>
                                 @endforeach                             
                            </select>
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Creditos</label>

                            <input type="number" value="10" id="creditos" name="creditos" placeholder="" class="form-control">
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Evaluar para Cerrar</label>
                            <select class="form-control" id="cerrar" name="cerrar">  
                                <option value="1">Si</option>  
                                <option value="2">No</option>                             
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">I Hora Semana</label>

                            <input type="number" value="10" id="ihs" name="ihs" placeholder="Intesidad Horaria Semanal" class="form-control">
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">I Hora Total</label>
                            <input type="number" value="10" id="iht" name="iht" placeholder="Intesidad Horaria Total" class="form-control">

                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
            </form>
        </div>
    </div>



    @endsection