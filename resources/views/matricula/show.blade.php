 @extends('layouts.plantilla') @section('title','Menu Listado') @section('content')
<script src="/js/consultaNotas.js"></script>

@if(Session::has('success'))
<div class="alert alert-info">
    {{Session::get('success')}}
</div>
@endif @php {{ $someArray = json_decode($datosCurso, true);}}@endphp

<div class="container">

    <div class="alert alert-success" role="alert">
        <h5 class="alert-heading">{{ $someArray[0]["nomCurso"]}} </h5>
        <p><b>Docente:</b> {{ $someArray[0]["nombre"]}} {{ $someArray[0]["nombre2"]}} {{ $someArray[0]["apellido"]}} {{ $someArray[0]["apellido2"]}}</p>
        <hr>
        <div class="row">
            <div class="col-6">
                <p class="mb-0"><b>Unidad Aprendizaje: </b> {{ $someArray[0]["nomUnidadAprendizaje"]}}</p>
            </div>
            <div class="col-3">
                <p> <b>Periodo:</b> {{$someArray[0]["nomPeriodo"]}} </p>
            </div>
            <div class="col-3">
                <p> <b>Estudiantes: {{sizeof($matriculados)}}</b> </p>
            </div>
        </div>

    </div>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">N°</th>
                <th scope="col">Documento</th>
                <th scope="col">Apellidos y Nombres</th>
                <th scope="col">Contacto</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($matriculados as $est)
            <tr>
                <td scope="row">{{$loop->index+1}}</td>
                <td scope="row">{{$est->username }} <input type="hidden" id="username" value="{{$est->username }}"></td>
                <td scope="row">{{$est->apellido }} {{$est->apellido2 }} {{$est->nombre }} {{$est->nombre2 }}</td>
                <td scope="row">{{$est->telefono }} --- {{$est->celular}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!--<div id="contenedor">..::..</div>-->
</div>
@endsection