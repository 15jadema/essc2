@extends('layouts.plantilla') @section('title','Menu Listado') @section('content')
<div class="container">
    <script src="/js/curso.js"></script>
    <div class="row">
        <div class="form-group col-4">
            <label for="exampleInputEmail1">Sede.</label>
            <select class="form-control" name="sede_id" id="sede_id">
                @foreach ($sede as $s)
                <option value="{{$s->id}}">{{$s->nomSede}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-4">
            <label for="exampleInputEmail1">Unidad de Aprendizaje</label>
            <select class="form-control" name="unidad_id" id="unidad_id">
                 @foreach ($unidad as $und)
                <option value="{{$und->id}}">{{$und->nomUnidadAprendizaje}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-4">
            <label for="exampleInputEmail1">Curso</label>
            <select class="form-control" name="curso_id" id="curso_id">            
            </select>
        </div>
    </div>

    <div class="row container" id="contenedor">
    @yield('contenedor')
    </div>
</div>
@endsection