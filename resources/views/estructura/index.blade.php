@extends('layouts.plantilla')
@section('title','Menu Listado')

@section('content')
<div class="container">
        <p>Estructura</p>
<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('estructura.create')}}" >Registrar Unidad Aprendizaje</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Documentos</th>
      <th scope="col">Apellidos y Nombres</th>      
      <th scope="col">Contacto</th>
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($estructura as $est)
    <tr>
      <td scope="row">{{$est->abreviatura }}</td>
      <td scope="row">{{$est->nomUnidadAprendizaje }}</td>
      <td scope="row">{{$est->estado_id }}</td>
      <td><a href="/student/{{$est->id}}" class="btn btn-primary">Detalle</a></td>
     </tr>
  @endforeach
  </tbody>
</table>
{!! $estructura->render() !!}
</div>
@endsection

