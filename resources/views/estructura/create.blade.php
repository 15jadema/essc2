@extends('layouts.plantilla') @section('title','Menu de Estructura') 
@section('content')
<!--
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/programa.js"></script>-->
<div class="container">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Crear Unidad de Aprendizaje</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/estructura" class="form-group" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Código</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Codigo">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input type="text" class="form-control" id="nomUnidadAprendizaje"  name="nomUnidadAprendizaje" aria-describedby="emailHelp" placeholder="Nombre">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Abreviación</label>
                            <input type="text" class="form-control" name="abreviatura" id="abreviatura" placeholder="Abreviación">

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Estado</label>
                           <select class="form-control" name="estado_id" id="estado_id">
                            @foreach ($estado as $es)
                <option value="{{$es->id}}">{{$es->nomEstado}}</option>
            @endforeach  
                           </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Practiva Laboral</label>
                            <select class="form-control" name="usarComoPractica" id="usarComoPractica">
                            <option value="0">No</option>
           
         </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Tipo de Evaluación</label>
                            <select class="form-control" name="tipoEvaluacion" id="tipoEvaluacion">
                            <option value="1">Individual</option>
           
         </select>
                        </div>
                    </div>
                                                     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
            </form>
        </div>
    </div>


    @endsection