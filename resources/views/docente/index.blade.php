@extends('layouts.plantilla')
@section('title','Menu Listado')

@section('content')

@if(Session::has('success'))
<div class="alert alert-info">
    {{Session::get('success')}}
</div>
@endif

<div class="container">
        <h4 class="text-primary">Docentes</h4>

<div class="mb-sm-2"><a class="btn btn-success float-right mb-xl-2" href="{{ Route('docente.create')}}" >Registrar Docente</a></div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Documentos</th>
      <th scope="col">Apellidos y Nombres</th>      
      <th scope="col">Contacto</th>
      <th scope="col">Estado</th>
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($docente as $doc)
    <tr>
      <td scope="row">{{$doc->username }}</td>
      <td scope="row">{{$doc->apellido}} {{$doc->apellido2 }} {{$doc->nombre }} {{$doc->nombre2 }}</td>
      <td scope="row">{{$doc->celular }} --- {{$doc->celular }}</td>
       <td>
                    @if($doc->status_id==2)
                    <strong class="text-success">Activo</strong>
                     @else
                    <strong class="text-danger">Inactivo</strong> 
                    @endif
                </td>
      <td><a href="/docente/{{$doc->id}}" class="btn btn-primary">Detalle</a></td>
       </tr>
  @endforeach
  </tbody>
</table>
{!! $docente->render() !!}
</div>
@endsection

