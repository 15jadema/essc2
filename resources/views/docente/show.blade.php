 @extends('layouts.plantilla') @section('title','Docente detalle') @section('content') @php {{ $someArray = json_decode($docente, true);}} @endphp


<script src="/js/consultaNotas.js"></script>
<div class="container mt-5">
    <div class="row">
        <div class="col-8">

        </div>

        <div class="col-2 text-right">
            <h5>{{ $someArray[0]["nombre"]}} {{ $someArray[0]["nombre2"]}}</h5>
            <h6>{{ $someArray[0]["apellido"]}} {{ $someArray[0]["apellido2"]}}</h6>
            <h6>Documento: {{ $someArray[0]["username"]}}</h6>
        </div>

        <div class="col-2">
            <div class="card" style="width:155px;">
                <img src="/images/{{ $someArray[0]['foto']}}">
            </div>

        </div>

    </div>

</div>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Información Personal</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Información</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

        <div class="row">
            <div class="col-3 mt-5">
                <p class="text-xl-left">Nombres: {{ $someArray[0]["nombre"]}} {{ $someArray[0]["nombre2"]}}</p>
                <p class="mt-1">Identificacion: {{ $someArray[0]["username"]}}</p>
                <p>Telefono:{{ $someArray[0]["telefono"]}} </p>
                <p>Correo: {{ $someArray[0]["email"]}}</p>
                <p>Profesión: {{ $someArray[0]["tituloAcademico"]}}</p>

            </div>
            <div class="col-3">

            </div>
            <div class="col-3 mt-5">
                <p class="text-xl-left">Apellidos: {{ $someArray[0]["apellido"]}} {{ $someArray[0]["apellido2"]}}</p>
                <p class="mt-1">Sexo: {{ $someArray[0]["sexo"]}}</p>
                <p>Celular: {{ $someArray[0]["celular"]}}</p>
                <p>Direccion: {{ $someArray[0]["direccion"]}}</p>
                <p>Barrio: {{ $someArray[0]["barrio"]}}</p>
            </div>
            <div class="col-3">
                <td><a href="/docente/{{$id}}/edit" class="btn btn-warning">Editar Docente</a></td>



            </div>
        </div>
    </div>
    <!--<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="row">
            <div class="col-3">
            </div>
        </div>
    </div>-->


</div>
</div>
</div>
@endsection