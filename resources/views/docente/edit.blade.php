@extends('layouts.plantilla') @section('title','Editar Docente') @section('content')
<script src="/js/municipios.js"></script>
<script src="/js/documento.js"></script>
<script src="/js/genero.js"></script>
<script src="/js/estado.js"></script>


<!--<script src="/js/programa.js"></script>-->

<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Docente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>

            <form id="delete" action="/docente/{{$docente->id}}" class="form-group" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }} @method('DELETE') Realmente desea <strong class="text text-danger">eliminar</strong> al docente: <b> {{$docente->apellidos}}  {{$docente->nombres}} </b> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Información Docente</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
            </div>
            <div class="modal-body">
                <form action="/docente/{{$docente->id}}" class="form-group" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }} @method('PUT') @csrf
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Primer Apellido</label>
                            <input type="text" class="form-control" name="apellido" id="apellidos" placeholder="Primer apellido" value="{{$docente->apellido}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Segundo Apellido</label>
                            <input type="text" class="form-control" name="apellido2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Segundo Apellido" value="{{$docente->apellido2}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Primer nombre</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Primer nombre" value="{{$docente->nombre}}">

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Segundo nombre</label>
                            <input type="text" class="form-control" name="nombre2" id="nombre2" aria-describedby="emailHelp" placeholder="Segundo nombre" value="{{$docente->nombre2}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Tipo Documento</label>
                            <input type="hidden" id="documento_id_text" value="{{$docente->documento_id}}" />
                            <select class="form-control" name="documento_id" id="documento_id">                                 
                            </select>

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Documento</label>
                            <input disabled type="number" class="form-control" name="username" id="username" placeholder="Documento" value="{{$docente->username}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label for="">Sexo</label>
                            <input type="hidden" id="genero_id_text" value="{{$docente->genero_id}}" />
                            <select class="form-control" name="genero_id" id="genero_id">
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Correo Electrónico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Correo electrónico" value="{{$docente->email}}">

                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Teléfono</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Teléfono" value="{{$docente->telefono}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Celular</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Celular" value="{{$docente->celular}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                            <input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento" value="{{$docente->fechaNacimiento}}">
                        </div>
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Municipio Nacimiento</label>
                            <input type="hidden" class="form-control" name="munNacimientoX_id" id="munNacimientoX_id" value="{{$docente->munNacimiento_id}}">
                            <select class="form-control" name="munNacimiento_id" id="munNacimiento_id">                            
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Dirección</label>
                            <input type="text" class="form-control" name="direccion" id="direccion" value="{{$docente->direccion}}">
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Barrio</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Barrio">
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Profesión</label>
                            <input type="text" class="form-control" name="tituloAcademico" id="tituloAcademico" value="{{$docente->tituloAcademico}}">
                        </div>

                    </div>
                    <div class="row">

                        <div class="form-group col-4">
                            <label for="foto">Fotografía</label>
                            <input type="file" class="form-control" name="foto">
                        </div>

                        <div class="form-group col-4">
                            <label for="exampleInputEmail1">Estado</label>

                            <input type="hidden" id="estado_id_text" value="{{$docente->status_id}}" />

                            <select class="form-control" name="status_id" id="status_id">                                 

                            </select>
                        </div>





                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Actualizar</button>
                </form>
                <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong">Eliminar</button>
            </div>
        </div>
    </div>

    @endsection