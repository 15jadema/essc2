<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',15)->unique();
            $table->string('apellido');
            $table->string('apellido2');
            $table->string('nombre');           
            $table->string('nombre2');
            $table->string('genero_id')->nullable();;
            $table->string('documento_id');
            $table->date('fechaNacimiento')->nullable();
            $table->bigInteger('depaNacimiento_id')->nullable();
            $table->bigInteger('munNacimiento_id')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();           
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('direccion')->nullable();
            $table->string('depaDireccion_id')->nullable();
            $table->bigInteger('munDireccion_id')->nullable();
            $table->date('fechaActualizacion')->nullable();
            $table->bigInteger('depaExpedicion_id')->nullable();
            $table->bigInteger('munExpedicion_id')->nullable();
            $table->bigInteger('estrato_id')->nullable();
            $table->bigInteger('medioTransporte_id')->nullable();;
            $table->bigInteger('sisben_id')->nullable();
            $table->bigInteger('rh_id')->nullable();
            $table->bigInteger('eps')->nullable();
            $table->bigInteger('zona_id')->nullable();
            $table->bigInteger('estadoCivil_id')->nullable();
            $table->string('nombreFamiliar')->nullable();
            $table->string('celularFamiliar')->nullable();;
            $table->bigInteger('relacionFamiliar_id')->nullable();
            $table->string('nombreInstitucion')->nullable();
            $table->bigInteger('depInstitucion_id')->nullable();
            $table->bigInteger('munInstitucion_id')->nullable();
            $table->bigInteger('nivelAcademico_id')->nullable();
            $table->string('tituloAcademico')->nullable();
            $table->boolean('graduado')->nullable();
            $table->integer('ultimoAno')->nullable();
            $table->string('foto')->nullable();
            $table->bigInteger('sede_id');
            $table->bigInteger('status_id')->nullable();
            $table->bigInteger('levelUser_id');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
