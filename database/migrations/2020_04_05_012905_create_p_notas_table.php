<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_notas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('curso_id');
            $table->bigInteger('unidadAprendizaje_id');
            $table->string('padre');
            $table->integer('porcentajePadre');
            $table->string('hijo');
            $table->integer('porcentajeHijo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_notas');
    }
}
