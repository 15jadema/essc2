<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nomPrograma");
            $table->string("codPrograma");
            $table->string("abreviatura");
            $table->string("numResolucion");
            $table->date("fechaResulucion");
            $table->boolean("preinscripcion");
            $table->boolean("aplicaGrupos");
            $table->string("tipoEvaluacion");
            $table->bigInteger('estado_id');
            $table->bigInteger("tipoPrograma");
            $table->bigInteger("sede_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas');
    }
}
