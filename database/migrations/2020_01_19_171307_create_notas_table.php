<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigoGrupo');
            $table->string('username');
            $table->bigInteger('unidadAprendizaje_id');
            $table->integer('corte');
            $table->integer('porcentaje');
            $table->string('descripcion');
            $table->decimal('nota', 3, 2);
            $table->integer('inasistencias');
            $table->string('observacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
