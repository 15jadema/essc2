<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsignarUnidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignar_unidads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('unidadAprendizaje_id');
            $table->bigInteger('nivel');
            $table->integer('ihs');
            $table->integer('iht');
            $table->boolean('cerrar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignar_unidads');
    }
}
