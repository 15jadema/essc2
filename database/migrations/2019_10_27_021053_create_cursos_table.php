<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo');
            $table->string('nomCurso');
            $table->bigInteger('docente_id');
            $table->bigInteger('jornada_id');
            $table->bigInteger('programa_id');
            $table->bigInteger('pensum_id');
            $table->bigInteger('unidadAprendizaje_id');
            $table->bigInteger('aula');
            $table->integer('cupoMax');
            $table->bigInteger('periodo_id');
            $table->date('fechaInicio');
            $table->date('fechaFinal');
            $table->bigInteger('estado_id');
            $table->bigInteger('archivado_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
